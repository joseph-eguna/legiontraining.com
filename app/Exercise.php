<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = [
        'name', 'super_set', 'set', 'repetitions', 'tempo_up', 'tempo_down',
         'short_of_failure', 'rest'
    ];

    /*public function Workout() {
        return $this->belongsTo('App\Workout', 'wk_id');

    }*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    /*public function getDescriptionAttribute($value)
    {
        return unserialize($value);
    }*/
}
