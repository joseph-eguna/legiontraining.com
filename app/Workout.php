<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    protected $fillable = [
        'name', 'description',
    ];

    /*public function IngredientCategory() {
        return $this->hasMany('App\IngredientCategory', 'ing_id');

    }*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    /*public function getDescriptionAttribute($value)
    {
        return unserialize($value);
    }*/
}
