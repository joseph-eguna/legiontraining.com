<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientCategory extends Model
{
    protected $fillable = [
        'ing_id', 'name', 'description'
    ];

    public function Ingredient() {
        return $this->belongsTo('App\Ingredient', 'id');
    }
}
