<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = [
        'name', 'description', 'unit', 'calories', 
        'substitutes', 'max_quantity', 'image_url'
    ];

    /*public function IngredientCategory() {
        return $this->hasMany('App\IngredientCategory', 'ing_id');

    }*/
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/
}
