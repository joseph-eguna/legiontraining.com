<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::auth();

Route::get('/', 'membersController@index');

Route::get('/dashboard', 'membersController@index');

Route::get('/dashboard/calendar', 'membersController@calendar');

Route::get('/dashboard/social', 'membersController@social');

Route::get('/admin', 'workoutsController@index');

//INGREDIENTS
Route::get('/admin/ingredients', 'ingredientsController@ingredients');

Route::post('/admin/ingredients', 'ingredientsController@ingredients_action_store');

Route::post('/admin/ingredients/update', 'ingredientsController@ingredients_action_update');

 Route::get('/admin/ingredients/delete', 'ingredientsController@ingredients_action_delete');

//INGREDIENTS CATEGORY
Route::post('/admin/ingredients/category', 'ingredientsController@ingredient_category_store');

Route::post('/admin/ingredients/delete-category', 'ingredientsController@ingredient_category_delete');

//INGREDIENTS IMAGE
Route::post('/admin/ingredients/upload', 'ingredientsController@ingredient_file_upload');

Route::post('/admin/ingredients/delete', 'ingredientsController@ingredient_image_delete');

//EXERCISES
Route::get('/admin/exercises', 'exercisesController@index');

Route::post('/admin/exercises', 'exercisesController@exercises_store');

Route::post('/admin/exercises/update', 'exercisesController@exercises_update');

Route::post('/admin/exercises/delete', 'exercisesController@exercises_delete');

//WORKOUT
Route::get('/admin/workouts', 'workoutsController@index');

Route::get('/admin/workouts/json', 'workoutsController@workout_getJSON_detail');

Route::post('/admin/workouts', 'workoutsController@workout_store');

Route::post('/admin/workouts/update', 'workoutsController@workout_update');

Route::post('/admin/workouts/delete', 'workoutsController@workout_delete');

//WORKOUT DAY
 Route::get('/admin/workouts/{id}/json', 'workoutsController@workout_day_getJSON_detail');

 Route::get('/admin/workouts/{wk_slug_id}', 'workoutsController@workout_days');

 Route::get('/admin/workouts/{wk_slug_id}/{day_id}', 'workoutsController@workout_days');

Route::post('/admin/workouts/{id}', 'workoutsController@workout_day_store');

Route::post('/admin/workouts/{id}/update', 'workoutsController@workout_day_update');

Route::post('/admin/workouts/{id}/delete', 'workoutsController@workout_day_delete');

Route::post('/admin/workouts/{id}/exercise', 'workoutsController@workout_day_store_exercise');

Route::post('/admin/workouts/{id}/exercise/update', 'workoutsController@workout_day_update_exercise');

Route::post('/admin/workouts/{id}/exercise/delete', 'workoutsController@workout_day_delete_exercise');

//Route::get('/admin/ingredients/{action}', 'membersController@ingredients_action');
//Route::get('/admin/ingredients/{action}/{id}', 'membersController@ingredients_action');




