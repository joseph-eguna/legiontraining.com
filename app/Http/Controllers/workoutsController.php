<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//use Storage;

use DB;

use Validator;


class workoutsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * View the list of workouts added.
     */
    public function index()
    {
    	
    	$workouts = DB::table('workouts')->orderBy('id','desc')->get();

        $ingredients = [];
        foreach(['protein','carbohydrates','fats','fruit_vege','condiments'] as $categories):
            
            $ingredients[$categories] = DB::table('ingredients')
                    
                    ->join('ingredient_relations', 
                           'ingredients.id', '=', 'ingredient_relations.ing_id')
                    ->join('ingredient_categories', 
                           'ingredient_categories.id', '=', 'ingredient_relations.cat_id')
                    
                    ->select('ingredients.id',
                             'ingredients.name',
                             'ingredients.calories', 
                             'ingredients.unit', 
                             'ingredient_categories.name as cat_name')
                    ->where('ingredient_categories.name', '=', $categories)
                    ->get();
        endforeach;
        //return $ingredients;

        return view('pages.admin-workouts', compact('workouts', 'ingredients'));
    }

    /*
     * Get workout detail per id for JSON result
     */
    public function workout_getJSON_detail(Request $request) {
        
        $workout_detail = DB::table('workouts')
                    
            ->join('workout_meals', 
                   'workouts.id', '=', 'workout_meals.wk_id')

            /*->join('ingredients', function ($join) {
                    $join->on('workout_meals.p1', '=', 'ingredients.id')
                         ->orOn('workout_meals.p2', '=', 'ingredients.id');
                })*/
            
            ->select('workouts.name as wk_name',
                     'workouts.description as wk_desc',
                     'workouts.url_rewrite as slug',
                     'workout_meals.*'
                     /*'ingredients.id as ing_id',
                     'ingredients.name as ing_name',
                     'ingredients.unit',
                     'ingredients.calories'*/
                     )

            ->where('workouts.url_rewrite', '=', $request->get('id'))
            ->get();

        return $workout_detail;

    }

    /*
     * Save workout data
     */
    public function workout_store(Request $request) {

        //validate main data
        $validator = Validator::make($request->all(), [
            'formdata.name'  => 'required|Min:3|unique:workout_meals,name'
        ]);
        if ($validator->fails()) 
            return $validator->errors()->all();

        $wk_id = DB::table('workouts')->insertGetId( [
                'name' => $request->get('formdata')['name'],
                'description' => $request->get('formdata')['description'],
                'url_rewrite' => strtolower(str_replace(' ', '-', $request->get('formdata')['name'])),
            ] );

        //insert meal tables
        $data = [];
        for($i=1;$i<7;$i++){

            $data[] = ['wk_id' => $wk_id, 
                 'meal_level' => 1,
                 'meal_order' => $i,
                 //'name' => $request->get('formdata')['name'],
                 //'description' => $request->get('formdata')['description'],
                 'p1' => $request->get('meal')['m'.$i]['protein-calorie']['prime']['id'],
                 'p2' => $request->get('meal')['m'.$i]['protein-calorie']['second']['id'],
                 'c1' => $request->get('meal')['m'.$i]['carbohydrate-calorie']['prime']['id'],
                 'c2' => $request->get('meal')['m'.$i]['carbohydrate-calorie']['second']['id'],
                 'f1' => $request->get('meal')['m'.$i]['fat-calorie']['prime']['id'],
                 'f2' => $request->get('meal')['m'.$i]['fat-calorie']['second']['id'],
                 'condiments_id' => $request->get('meal')['m'.$i]['condiment']['prime']['id'],
                 'vegetables_id' => $request->get('meal')['m'.$i]['fruit-vege']['prime']['id'],
                 'youtube_id' => $request->get('meal')['m'.$i]['video-id']['youtube'],
                 'vimeo_id'   => $request->get('meal')['m'.$i]['video-id']['vimeo'],
                 'tdc_p' => $request->get('nutrients')['active-tdc']['protein'],
                 'tdc_c' => $request->get('nutrients')['active-tdc']['carbs'],
                 'tdc_f' => $request->get('nutrients')['active-tdc']['fats'],
                 'ml_p' => $request->get('nutrients')['m'.$i]['protein'],
                 'ml_c' => $request->get('nutrients')['m'.$i]['carbs'],
                 'ml_f' => $request->get('nutrients')['m'.$i]['fats']
            ];

        }
        DB::table('workout_meals')->insert( $data );

        return \Response::json(['success'=>true]);

    }

    /*
     * Update workout data
     */
    public function workout_update(Request $request) {
        
        //validate if workout url rewrite column exists
        $validator = Validator::make($request->all(), [
            'formdata.id'  => 'exists:workouts,url_rewrite'
        ],['exists' => 'Data does not exists']);

        if ($validator->fails()) 
            return $validator->errors()->all();

        //insert to main table : workouts
        DB::table('workouts')
                ->where('url_rewrite', $request->get('formdata')['id'])
                ->update( [
                    'name' => $request->get('formdata')['name'],
                    'description' => $request->get('formdata')['description']
                ] );

        //get the primary id, where url_rewrite column is equal to:
        $wk_id = DB::table('workouts')->select('id')
                ->where('url_rewrite', $request->get('formdata')['id'])->get();


        //Insert into Meals table
        for($i=1;$i<7;$i++){
           
            DB::table('workout_meals')
                ->where([ 
                          ['wk_id','=',$wk_id[0]->id],
                          ['meal_order','=',$i]
                       ])

                ->update(['p1' => $request->get('meal')['m'.$i]['protein-calorie']['prime']['id'],
                         'p2' => $request->get('meal')['m'.$i]['protein-calorie']['second']['id'],
                         'c1' => $request->get('meal')['m'.$i]['carbohydrate-calorie']['prime']['id'],
                         'c2' => $request->get('meal')['m'.$i]['carbohydrate-calorie']['second']['id'],
                         'f1' => $request->get('meal')['m'.$i]['fat-calorie']['prime']['id'],
                         'f2' => $request->get('meal')['m'.$i]['fat-calorie']['second']['id'],
                         'condiments_id' => $request->get('meal')['m'.$i]['condiment']['prime']['id'],
                         'vegetables_id' => $request->get('meal')['m'.$i]['fruit-vege']['prime']['id'],
                         'youtube_id' => $request->get('meal')['m'.$i]['video-id']['youtube'],
                         'vimeo_id'   => $request->get('meal')['m'.$i]['video-id']['vimeo'],
                         'tdc_p' => $request->get('nutrients')['active-tdc']['protein'],
                         'tdc_c' => $request->get('nutrients')['active-tdc']['carbs'],
                         'tdc_f' => $request->get('nutrients')['active-tdc']['fats'],
                         'ml_p' => $request->get('nutrients')['m'.$i]['protein'],
                         'ml_c' => $request->get('nutrients')['m'.$i]['carbs'],
                         'ml_f' => $request->get('nutrients')['m'.$i]['fats']
                          ]);  
        }

        //return $request->all();
        
        return \Response::json(['success'=>true]);
    }


    /*
     * Delete program data
     */
    public function workout_delete(Request $request) {
        
        //return $request->all();
        //validate if workout url rewrite column exists
        $validator = Validator::make($request->all(), [
            'slug_id'  => 'exists:workouts,url_rewrite'
        ],['exists' => 'Data does not exists']);

        if ($validator->fails()) 
            return $validator->errors()->all();

        DB::table('workouts')->where('url_rewrite', $request->get('slug_id'))->delete();

        return \Response::json(['success'=>true]);
    }

    /*
     * View the number of days for a certain workout
     */
    public function workout_days($wk_slug_id, $day_id=false)
    {

        
        $exercises = DB::table('exercises')->get();

        $day_exercises = DB::table('workout_day_exercises')
                
                ->join('exercises', 'workout_day_exercises.ex_id', '=', 'exercises.id')

                ->select('exercises.name', 'exercises.video_id', 'workout_day_exercises.*')

                ->where('workout_day_exercises.wkd_id', $day_id)

                ->get(); 


        $workout_days = DB::table('workout_days')

                ->where('wk_slug_id', $wk_slug_id)

                ->orderBy('id', 'desc')->get();

        $name = $wk_slug_id;
    	/*where('active', 1)
               ->orderBy('name', 'desc')
               ->take(10)
               ->get();*/

    	//$workout_day = \App\Workout::where('');

        //$ingredients['protein'] = \App\ingredients::()
        /*DB::table('users')
            ->join('contacts', 'users.id', '=', 'contacts.user_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();*/

        return view('pages.admin-workout-days', compact('name', 'workout_days', 'day_exercises', 'exercises'));
    }

    /*
     * Get workout day data in JSON format
     */
    public function workout_day_getJSON_detail(Request $request) {

        //return $request->all();

        $validator = Validator::make($request->all(), [ 
                'id' => 'exists:workout_days,id'
            ]);

        if($validator->fails())
            return $validator->errors()->all();

        $select = DB::table('workout_days')->where('id', $request->get('id'))->get();
        
        return \Response::json( ['success'=>true, 'data'=>$select[0]] );
    }

    /*
     * Workout day data store
     */
    public function workout_day_store($id, Request $request) {

        //return $request->all();

        $validator = Validator::make([ 'wk_slug_id'=>$id, 'form' => $request->all() ], [
                'wk_slug_id' => 'exists:workouts,url_rewrite',
                'form.name' => 'required'
            ]);

        if($validator->fails())
            return $validator->errors()->all();

        $insert_id = DB::table('workout_days')->insertGetId([
                'wk_slug_id' => $id,
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'email_subject' => $request->get('email_subject'),
                'email_content' => $request->get('email_content')
            ]);

        return \Response::json(['success'=>true, 'id' => $insert_id]);
    }

    /*
     * Workout day data update
     */
    public function workout_day_update($id, Request $request) {

        //return $request->all();

        $validator = Validator::make(  $request->all(), [
                'id' => 'exists:workout_days,id']);

        if($validator->fails())
            return $validator->errors()->all();

        DB::table('workout_days')
            ->where('id', $request->get('id'))
            ->update([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'email_subject' => $request->get('email_subject'),
                'email_content' => $request->get('email_content')
            ]);

        return \Response::json(['success'=>true]);
    }

    /*
     * Workout day data delete
     */
    public function workout_day_delete($id, Request $request) {

        //return $request->all();

        $validator = Validator::make(  $request->all(), [
                'id' => 'exists:workout_days,id']);

        if($validator->fails())
            return $validator->errors()->all();

        DB::table('workout_days')->where('id', $request->get('id'))->delete();

        return \Response::json(['success'=>true]);
    }

    /*
     * Workout day exercises store
     */
    public function workout_day_store_exercise($id, Request $request) {

        //return $id;
        //return $request->all();

        $validator = Validator::make($request->all(),[
                'wkd_id' => 'exists:workout_days,id'
            ], ['exists' => 'Data does not exist']); 

        if ($validator->fails()) 
            return $validator->errors()->all();

        $detail = DB::table('exercises')->where( 'id', $request->get('id') )->get();
        
        DB::table('workout_day_exercises')->insertGetId(
            [
                'ex_id' => $request->get('id'),
                'wkd_id' => $request->get('wkd_id'),
                'super_set'     => $detail[0]->super_set,
                'set'           => $detail[0]->set,
                'repetitions'   => $detail[0]->repetitions,
                'tempo_up'      => $detail[0]->tempo_up,
                'tempo_down'    => $detail[0]->tempo_down,
                'short_of_failure' => $detail[0]->short_of_failure,
                'rest' => $detail[0]->rest
            ]);

        return \Response::json(['success'=>true]);
    }

    /*
     * Workout day exercises update
     */
    public function workout_day_update_exercise($id, Request $request) {

        //return $id;
        //return $request->all();

        $validator = Validator::make($request->all(),[
                'id' => 'exists:workout_day_exercises,id',
                'key' => 'in:super_set,set,repetitions,tempo_up,tempo_down,short_of_failure,rest',
                'value' => 'required'

            ], ['exists' => 'Data does not exist', 'in' => 'key not found in the table']); 

        if ($validator->fails()) 
            return $validator->errors()->all();

        DB::table('workout_day_exercises')
            ->where('id', $request->get('id'))
            ->update([ $request->get('key') => $request->get('value') ]);

        /*->insertGetId(
            [
                'ex_id' => $request->get('id'),
                'wkd_id' => $request->get('wkd_id'),
                'super_set'     => $detail[0]->super_set,
                'set'           => $detail[0]->set,
                'repetitions'   => $detail[0]->repetitions,
                'tempo_up'      => $detail[0]->tempo_up,
                'tempo_down'    => $detail[0]->tempo_down,
                'short_of_failure' => $detail[0]->short_of_failure,
                'rest' => $detail[0]->rest
            ]);


         /*DB::table('workout_meals')
                ->where([ 
                          ['wk_id','=',$wk_id[0]->id],
                          ['meal_order','=',$i]
                       ])

                ->update(['p1' => $request->get('meal')['m'.$i]['protein-calorie']['prime']['id']*/


        return \Response::json(['success'=>true]);
    }

    /*
     * Workout day exercises delete
     */
    public function workout_day_delete_exercise($id, Request $request) {

        //return $request->all();

        $validator = Validator::make($request->all(),[
                'id' => 'exists:workout_day_exercises,id'
            ], ['exists' => 'Data does not exist']); 

        if ($validator->fails()) 
            return $validator->errors()->all();

        DB::table('workout_day_exercises')
            ->where('id', $request->get('id'))
            ->delete();

        return \Response::json(['success'=>true]);
    }
}
