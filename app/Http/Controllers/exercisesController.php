<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Storage;

use DB;

use Validator;


class exercisesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$exercises = DB::table('exercises')->orderBy('id', 'desc')->get();

    	return view('pages.admin-exercises', compact('exercises'));
    }

    public function exercises_store(Request $request){

        //validate main data
        $validator = Validator::make($request->all(), [
            'formdata.name'  => 'required|Min:3|unique:exercises,name',
            'formdata.set' 	 => 'required',
            'formdata.repetitions' => 'required',
            'formdata.tempo_up'	   => 'required',
            'formdata.tempo_down'  => 'required',
            'formdata.short_of_failure' => 'required',
            'formdata.rest' => 'required',
        ]);
        if ($validator->fails()) 
            return $validator->errors()->all();

        //return $request->get('formdata')['super_set'];
        DB::table('exercises')->insert( [
                'name' 		=> $request->get('formdata')['name'],
                'super_set' => $request->get('formdata')['super_set'],
                'set' 		=> $request->get('formdata')['set'],
                'repetitions' => $request->get('formdata')['repetitions'],
                'tempo_up' 	  => $request->get('formdata')['tempo_up'],
                'tempo_down'  => $request->get('formdata')['tempo_down'],
                'short_of_failure' => $request->get('formdata')['short_of_failure'],
                'rest' 			   => $request->get('formdata')['rest'],
                'video_id'         => $request->get('formdata')['video-id']
            ] );

       	return \Response::json(['success'=>true]);
    }

    public function exercises_update(Request $request){

    	$validator = Validator::make($request->all(), [
    		'data_id' => 'numeric|exists:exercises,id',
    		'key' => 'required',
    		'value' => 'required'
    	]);

    	if ($validator->fails()) 
            return $validator->errors()->all();

        DB::table('exercises')
        	->where('id', $request->get('data_id'))
        	->update( [$request->get('key') => $request->get('value')] );


    	return \Response::json(['success'=>true]);
	}

	public function exercises_delete(Request $request){

	 	$validator = Validator::make($request->all(), [
    		'data_id' => 'numeric|exists:exercises,id'
    	]);

    	if ($validator->fails()) 
            return $validator->errors()->all();

        DB::table('exercises')
        	->where('id', $request->get('data_id'))
        	->delete();
	 	

	 	return \Response::json(['success'=>true]);
	 }
}
