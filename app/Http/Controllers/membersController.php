<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Storage;

use DB;

use Validator;


class membersController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('pages.members-dashboard');
    }
    
    public function calendar()
    {
        return view('pages.members-calendar');
    }

    public function social()
    {
        return view('pages.members-social');
    }

    public function admin()
    {
        return view('pages.admin');
    }
}
