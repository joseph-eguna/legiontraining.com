<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Ingredient;

use Storage;

use DB;

use Validator;



class ingredientsController extends Controller
{
    
    var $units = ['g', 'ml', 'scoop', 'slice', 'each'];

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * List of Ingredients
     */
    public function ingredients() 
    {   

        //Storage::allFiles('/'));
        
        //return \App\Llibrary\cdn_upload_api::upload_to_cloud();
        
        $images = array_reverse( Storage::allFiles('/ingredients') );

        $ingredients = DB::table('ingredients')->orderBy('id', 'desc')->get();

        $categories  = DB::table('ingredient_categories')
                        ->groupBy('name')
                        ->get();

        $units = $this->units; 

        return view('pages.admin-ingredients', compact('images', 'categories', 'units', 'ingredients'));
    
    }

    /*
     * Store Ingredients added.
     */
    public function ingredients_action_store(Request $request) {

        //return $request->get('image_url');

        $validator = Validator::make($request->all(), [
            'name'  => 'required|Min:3|unique:ingredients,name',
            'unit'=>'required',
            'calorie'=>'required',
            'max_qty'=>'required',
            'categories'=>'array'
        ]);

        if ($validator->fails()) 
            return $validator->errors()->all();
        
        $insert_id = DB::table('ingredients')->insertGetId( 
            [ 'name'         => $request->get('name'),
            'description'  => $request->get('description'),
            'image_url'    => $request->get('image_url'),
            'unit'         => $request->get('unit'),
            'categories'   => serialize($request->get('categories')),
            'calories'     => $request->get('calorie'),
            'max_quantity' => $request->get('max_qty')] );

        if($insert_id > 0) {

            $cat = [];
            if(!empty($request->get('categories'))){
                foreach($request->get('categories') as $c) {
                    $cat[] = ['ing_id' => $insert_id, 'cat_id'=>$c];
                }
            }
            DB::table('ingredient_relations')->insert($cat);
        }

        return \Response::json(['success'=>true]);

        //return back()->with(['message' => 'Successfully Added ' . $request->get('name')]);*/
    }

    /*
     * Update Ingredients added.
     */
    public function ingredients_action_update(Request $request) {

        //return $request->get('image_url');

        $validator = Validator::make($request->all(), [
            'id'  => 'exists:ingredients,id',
            'unit'=>'required',
            'calorie'=>'required',
            'max_qty'=>'required',
            'categories'=>'array'
        ]);

        if ($validator->fails()) 
            return $validator->errors()->all();
        
        
        DB::table('ingredients')
            ->where('id', $request->get('id'))
            ->update( 
            [ 'name'         => $request->get('name'),
            'description'  => $request->get('description'),
            'image_url'    => $request->get('image_url'),
            'unit'         => $request->get('unit'),
            'categories'   => serialize($request->get('categories')),
            'calories'     => $request->get('calorie'),
            'max_quantity' => $request->get('max_qty')] );

        DB::table('ingredient_relations')->where('ing_id', $request->get('id'))->delete();
    
        $cat = [];
        if(!empty($request->get('categories'))){
            foreach($request->get('categories') as $c) {
                $cat[] = ['ing_id' => $request->get('id'), 'cat_id'=>$c];
            }
        }
        DB::table('ingredient_relations')->insert($cat);
        
        return \Response::json(['success'=>true]);

        //return back()->with(['message' => 'Successfully Added ' . $request->get('name')]);*/
    }

    /*public function ingredients_action_update(Request $Request) {

        $validator = Validator::make($Request->all(), [
            'name'  => 'required|Min:3|unique:ingredients,name',
            'unit'=>'required',
            'calorie'=>'required',
            'max_qty'=>'required'
        ]);

        if ($validator->fails()) 
            return $validator->errors()->all();
        
        else {
            
            $update =  Ingredient::where('id', $Request->get('id'))
                    ->update( [ 'name'  => $Request->get('name'),
                                'description'  => $Request->get('description'),
                                'image_url'    => $Request->get('image_url'),
                                'unit'         => $Request->get('unit'),
                                'categories'   => serialize($Request->get('categories')),
                                'calories'     => $Request->get('calorie'),
                                'max_quantity' => $Request->get('max_qty')] );

        }

        return ['message'=>'success'];

    }*/


    public function ingredients_action_delete(Request $request) {

        //Ingredient::where('id', $request->get('id'))->delete();

        DB::table('ingredients')->where('id', $request->get('id'))->delete();

        DB::table('ingredient_relations')->where('ing_id', $request->get('id'))->delete();

        return \Response::json(['success'=>true]);

    }

    /*
     * Upload Ingredient Image 
     */
    public function ingredient_file_upload(Request $Request) {
        
        $file = $Request->file('photo'); 

        if ($file->isValid()) {
            
            
            if(!in_array('ingredients', Storage::directories('/')))
            
                Storage::makeDirectory('ingredients');

            Storage::put('ingredients/' . strtotime("now"), file_get_contents($file));

            return Storage::url( 'app/ingredients/' . $file->getClientOriginalName() );
        }

        return false;
    }

    /*
     * Store Ingredients Category
     */
    public function ingredient_category_store(Request $Request) {
        
        $validator = Validator::make($Request->all(), [
            'category'  => 'unique:ingredient_categories,name',
        ]);

        if ($validator->fails()) 
            return $validator->errors()->all();

        if($Request->get('category'))
            \App\IngredientCategory::create(['ing_id'=> 0, 'name'=> $Request->get('category')]);

        return ['message'=>'success'];
    }

    /*
     * Store Ingredients Category
     */
    public function ingredient_category_delete(Request $Request) {
        
        //Ingredient::where('id', $Request->get('id'))->delete();

        DB::table('ingredient_categories')->where('id', '=', $Request->get('id'))->delete();

        return ['message'=>'success'];
    }

    /*
     * Delete Ingredients Image
     */
    public function ingredient_image_delete(Request $Request) {
        
        //return $Request->all();

        Storage::delete( $Request->get('img_url') );

        return ['message'=>'success'];

    }
}
