<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutDay extends Model
{
    protected $fillable = [
        'name', 'description',
    ];

    public function Workout() {
        return $this->belongsTo('App\Workout', 'wk_id');

    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/

    /*public function getDescriptionAttribute($value)
    {
        return unserialize($value);
    }*/
}
