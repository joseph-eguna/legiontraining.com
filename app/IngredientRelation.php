<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IngredientRelation extends Model
{
    protected $fillable = [
        'ing_id', 'cat_id', 'name'
    ];


}
