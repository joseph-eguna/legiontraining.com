
//Show Modal For Adding New Exercises
$('body').on('click', '.modal-popup, .delete-notify', function(e){
	
	e.preventDefault();

	var _thisevent = $(this);
	
	$( _thisevent.attr('data-target') ).modal('show')

	if(_thisevent.hasClass('delete-notify')){

		dataId = _thisevent.closest('.panel').attr('data-id');
		
		$( _thisevent.attr('data-target') ).attr('data-id', dataId);
		
		if(_thisevent.hasClass('wk_day'))
		
			$( _thisevent.attr('data-target') ).addClass('wk_day');
		
		else
		
			$( _thisevent.attr('data-target') ).removeClass('wk_day');

	} else {

		if(_thisevent.attr('data-id'))
		
			workout_day.formImplement($(this), true);
		
		else
			workout_day.formReset($(this));

	}
});

//ON SELECT EXERCISES
/*$('body').on('change', '.select-exercise select', function(e){

	e.preventDefault();

	var _this = $(this);

	workout_day.selectExercise($(this));

});*/

//ADD NEW EXERCISES
$('body').on('click', '#add-exercise', function(e){

	e.preventDefault();

	var _this = $(this);

	select = $('.'+_this.attr('data-target')+' select');
	
	//console.log(.val());

	workout_day.selectExercise( select );

});

//ON DATA CHANGE CHECKBOX FOR EXERCISE
$('body').on('change', '#day-exercise-list .panel-body input[type="checkbox"]', function(e){

	e.preventDefault();

	_thisevent = $(this);
	
	workout_day.updateExercise( $(this), _thisevent.attr('name'), ((_thisevent.is(':checked')) ? 'on' : 0) );	
});

//ON DATA CHANGE SELECT FOR EXERCISE
$('body').on('click', '#day-exercise-list .panel-body .cs-wrapper', function(e){

	e.preventDefault();

	_thisevent = $(this);
	
	//console.log(_thisevent.find('select').attr('name')); 

	//console.log(_thisevent.find('select').val()); 
	workout_day.updateExercise( _thisevent, _thisevent.find('select').attr('name'), _thisevent.find('select').val() );	
});

//ON SUBMIT FORM 
$('body').on('submit', '.workouts-action', function(e){

	e.preventDefault();
	//console.log('test');

	workout_day.formSubmit($(this), true);

})

//CONFIRM DELETE NOTIFICATION OPTION
$('body').on('click', '#alert-notification input[type="submit"]', function(e){
	
	e.preventDefault();

	var _thisevent = $(this);
	
	if(_thisevent.closest('#alert-notification').hasClass('wk_day')) {

		workout_day.delete(_thisevent);

	} /*else {

		workout_day.deleteExercise(_thisevent);

	}*/
});

//DELETE EXERCISE
$('body').on('click', '.delete-exercise', function(e){
	
	e.preventDefault();

	var _thisevent = $(this);
	
	id = _thisevent.closest('.panel-default').attr('data-id');

	workout_day.deleteExercise(_thisevent, id);

});

var workout_day = {
	
	variable : {
		_token : $('form.workouts-action input[name="_token"]').val(),
		inserturl : document.URL

	},
	
	delete : function(_thisevent){

		_this = this;

		data = {
			_token : _this.variable._token,
			id: _thisevent.closest('#alert-notification').attr('data-id')
		};

		$.post(_this.variable.inserturl+'/delete', data, function(response){

			//console.log(response);

			//$('.workout-day.day-'+data.id).fadeOut();

			if(response.success) {

				window.location.href="";
			}
		});
	},

	selectExercise : function(_thisevent){

		_this = this;

		_this.formSubmit(_thisevent.closest('form'));

		setTimeout(function(){

			reference_id = parseInt(_thisevent.closest('form').attr('data-id'));
			data = {
				_token : _this.variable._token,
				'id': _thisevent.val(),
				'wkd_id' : reference_id
			};

			//console.log( reference_id );
			$.post(_this.variable.inserturl + '/exercise', data, function(response){

				//console.log(response);

				workout_day.formImplement(_thisevent.closest('form'), false);

				$('#modal-slide-up').animate({scrollTop: $(window).height() }, '500', 'swing');

			});

		}, 800);
		
		//console.log(id);

		/*$.post(_this.variable.inserturl + '/exercise', data, function(response){

			console.log(response);
		});*/
	},

	updateExercise : function(_thisevent, key, value){

		_this = this;

		data = {
			_token : _this.variable._token,
			id: _thisevent.closest('.panel-default').attr('data-exercise-id'),
			key : key,
			value : value
		};

		//console.log(data);
		$.post(_this.variable.inserturl+'/exercise/update', data, function(response){

			console.log(response);
		});
	},

	deleteExercise : function(_thisevent, id){

		_this = this;

		//id = (id) ? id : _thisevent.closest('#alert-notification').attr('data-id');
		data = {
			_token : _this.variable._token,
			id: id
		};

		//console.log(data);
		$.post(_this.variable.inserturl+'/exercise/delete', data, function(response){

			
			if(response.success) {

				$('#day-exercise-'+data.id).closest('.panel-default').css({'border': '2px dashed red'}).delay('1000').fadeOut();
			}
			console.log(response);
		});
	},

	formSubmit : function(_thisevent, refresh){
		
		_this = this;

		data = {
			_token : _this.variable._token,
			name: _thisevent.find('input[name="name"]').val(),
			description : $('#description').code(), 
			email_subject : _thisevent.find('input[name="email_subject"]').val(),
			email_content : $('#email-content').code(),
			id: _thisevent.attr('data-id')
		}; 


		//for updating
		url = (_thisevent.attr('data-id')) ? _this.variable.inserturl + '/update' : _this.variable.inserturl;

		$.post(url, data, function(response){

			if(response.success) {
		
				_thisevent.attr('data-id', response.id)
				if(refresh)
					window.location.href = "";
				
			} else {

				_this.responseHTML(response, _thisevent.closest('form'));
			}

			//console.log(response);
		});	

	},

	formImplement: function (_thisevent, onclick){

		_this = this;

		//console.log(  _this.variable.inserturl + '/json' );

		if(onclick) {
			
			theForm = $(_thisevent.attr('data-target')).find('.workouts-action');

			theForm.attr('data-id', _thisevent.attr('data-id'));
			
			$.get( _this.variable.inserturl + '/json', {
				id : _thisevent.attr('data-id')
			}, function(response){

				//console.log(response);

				if(response.success)
				{
					
					theForm.find('input[name="name"]').val( response.data.name );
					theForm.find('input[name="email_subject"]').val( response.data.email_subject );
					theForm.find('#description').code( response.data.description );
					theForm.find('#email-content').code( response.data.email_content );
				}

			});
		}
		
		$('#day-exercise-list').load( _this.variable.inserturl+'/'+_thisevent.attr('data-id')+' #day-exercise-list > *', function(){
			
			$.Pages.init();

			$('#day-exercise-list .cs-wrapper select').each(function(){

				_thisevent = $(this);

				_thisevent.closest('.cs-wrapper').find('.cs-placeholder').text( _thisevent.val() );

			});
		});


		//console.log('update');

			

	},

	formReset: function (_thisevent){

		_this = this;

		$(_thisevent.attr('data-target')).find('.workouts-action').removeAttr('data-id');

		$('#day-exercise-list').html('');

		theForm = $(_thisevent.attr('data-target')).find('.workouts-action');

		theForm.find('input[name="name"]').val('');
		theForm.find('input[name="email_subject"]').val('');
		theForm.find('#description').code('');
		theForm.find('#email-content').code('');
	},

	responseHTML : function(responseJSON, target){

		_this = this;
		_html = '<p class="alert alert-danger">';

		target.find('.response').html('');

		if($.isArray(responseJSON)) {

			$.each(responseJSON, function(i, v){
				_html += v + '<br />';
			});
		_html += '</p>';

		target.find('.response').html(_html);

		} else {
			alert('Error Alert;');
		}
	}
}


$('[data-toggle="popover"]').popover({
	html: true
});


$(window).load(function(){

	setTimeout(function(){

		/*$('.video-display').each(function(){

			_thisevent = $(this);
			_id = _thisevent.attr('data-video-id');

			_thisevent.html( '<iframe src="http://player.vimeo.com/video/'+_id+'" frameborder="0" allowfullscreen="" style="width:100%"></iframe>' )
		
		});*/
		//http://www.bodybuildingestore.com/wp-content/uploads/2012/06/Workouts-of-Advanced-Bodybuilders.jpg

		$('img.lazy-loading').each(function(){

			$(this).attr('src', 'http://www.bodybuildingestore.com/wp-content/uploads/2012/06/Workouts-of-Advanced-Bodybuilders.jpg');
		});

		//('src', $(this).attr('data-src'));

	}, 2000);
		
});