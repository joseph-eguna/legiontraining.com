//TOOLTIP POPOVER
 $('[data-toggle="popover"]').popover({
 	trigger: 'hover',
 	html: true
 });

//SHOW MODAL FOR NEW PROGRAM
$('body').on('click', '.modal-popup', function(e){
	
	e.preventDefault();

	var _this = $(this);
	
	$( _this.attr('data-target') ).modal('show')

	$( _this.attr('data-target') ).find('.response').html('');


	if (typeof _this.attr('data-id') != "undefined") {
		
		programForm.implement(_this.attr('data-id'), $( _this.attr('data-target') ).find('form') );

	} else {

		programForm.reset( $( _this.attr('data-target') ).find('form') );
	}

});

//DELETE NOTIFICATION PROGRAM
$('body').on('click', '.notify-delete', function(e){
	
	e.preventDefault();

	var _this = $(this);
	
	$( _this.attr('data-target') ).attr( { 'data-id' : _this.attr('data-id'),
										   'data-table' : _this.attr('data-table'),
										   'data-target-delete': _this.attr('data-target-delete')}); 

	$( _this.attr('data-target') ).modal('show')

});

//DELETE CONFIRMATION PROGRAM
$('body').on('click', '#alert-notification input[type="submit"]', function(e){
	
	e.preventDefault();

	var _this = $(this);
	
	programForm.delete(_this);

});

//ON CHANGE MEAL INGREDIENT
$('body').on('click', '.meal-plan .cs-wrapper', function(e){

	e.preventDefault();

	var _this = $(this);

	mealCalculation.calculation(_this);

});

//On Sudmit Workout Page
$('body').on('submit', '.workouts-action', function(e){

	e.preventDefault();

	var _this = $(this);

	mealData = {};
	$.each([1,2,3,4,5,6], function(i,v){

		mealData['m'+v] = {};
		$.each(['protein-calorie','carbohydrate-calorie','fat-calorie',
				'fruit-vege','condiment', 'video-id'], function(ii,vv){

			mealData['m'+v][vv] = {};
			$.each(['prime','second'], function(iii,vvv){
				
					vaLue = $('#meal-'+v+' .'+vv+' .'+vvv+' select').val(); 
					vaLue = String(vaLue).split('|'); 
					vaLue = { 'cal': vaLue[0], 'id': vaLue[1], 'unit': vaLue[2], }

				mealData['m'+v][vv][vvv] = vaLue;
			});

			mealData['m'+v][vv]['youtube'] = $('#meal-'+v+' .'+vv+' input[name="youtube_id"]').val();
			mealData['m'+v][vv]['vimeo'] = $('#meal-'+v+' .'+vv+' input[name="vimeo_id"]').val();
		});
	});
	//console.log(mealData); 

	nutData = {}
	$('#add-macronutrients .macro-nutrients tr').each(function(){
		
		_thisNut = $(this);
		
		_thisData  = String(_thisNut.attr('class')).replace(/\s/g , "-");
		
		nutData[_thisData] = {
			'protein' : _thisNut.find('.p').val(),
			'carbs' : _thisNut.find('.c').val(),
			'fats' : _thisNut.find('.f').val(),
		}
	});
	//console.log(nutData); 

	data = {
		_token: _this.find('input[name="_token"]').val(), 
		formdata: {
			id : (typeof _this.attr('data-id') == 'undefined') ? 0 : _this.attr('data-id'),
			name : $('#main-wk-description input[name="name"]').val(),
			description : $('#description').code()
		},
		meal: mealData,
		nutrients: nutData,

	};

	//console.log(data);
	$.post( _this.attr('action') , data, function(response) {

		$('#modal-slide-up').animate({scrollTop:0}, '1000', 'swing');

		console.log(response);

		if(response.success) {
			_this.find('.response').html('<p class="alert alert-success"> Success! </p>');
			setTimeout(function(){
				window.location.href = "";
			}, 1000);
		} else {
			
			error = '<p class="alert alert-danger">';
			$.each(response, function(i,v){
				error +=  v + ' <br />';
			});
			error += '</p>';
			_this.find('.response').html( error );
		}
	});

});


//GENERATE DATA TO WORKOUT FORM
var programForm = {
	implement : function(id, form){
		$.get('/admin/workouts/json', { 'id' : id }, function(response){
			
			console.log(response);

			if(response.length > 0){
				$.each(response, function(i,v){
					
					//console.log(v);

					$.each(v, function(ii, vv){

						//console.log(ii);

						$.each({
							p1: '.protein-calorie .prime',
							p2: '.protein-calorie .second',
							c1: '.carbohydrate-calorie .prime',
							c2: '.carbohydrate-calorie .second',
							f1: '.fat-calorie .prime',
							f2: '.fat-calorie .second',
							vegetables_id: '.fruit-vege .prime',
							condiments_id: '.condiment .prime'
						}, function(iii,vvv){

							if(ii == iii){

								//console.log('#meal-'+(i+1)+ ' ' +vvv +' select option');
								$('#meal-'+(i+1)+ ' ' +vvv +' select option').each(function(){
									
									selectValue = String($(this).val()).split('|');
									if(selectValue[1] == vv){

											
										//2000 was a default calorie
										ing_cal = '';
										switch( $(this).closest('tr').attr('class') ) {
										    case 'protein-calorie':

										        tdc = parseInt(2000 * (v.tdc_p/100));
												ing_cal = ([tdc * (v.ml_p / 100)]/parseInt(selectValue[0])).toFixed(2);
										        break;
										    case 'carbohydrate-calorie':
										        
										        tdc = parseInt(2000 * (v.tdc_c/100));
												ing_cal = ([tdc * (v.ml_c / 100)]/parseInt(selectValue[0])).toFixed(2);
										        break;
										    default:
										        ing_cal =  selectValue[0] + ' cal/';
										}
										//console.log($(this).val());

										$(this).closest('td').find('.help').text( ing_cal+' '+selectValue[2] );
										$(this).closest('td').find('.cs-placeholder').text( $(this).text() );
										$(this).closest('td').find(' select').val($(this).val());
									}
								});
							}
						});

						//nutrients data
						if(['tdc_p','tdc_c','tdc_f', 
							'ml_p', 'ml_c', 'ml_f'].indexOf(ii) != -1) {

							nutClass = String(ii).split('_'); 
							console.log(vv);
							
							if(nutClass[0] == 'tdc')
								$('#add-macronutrients tr.'+nutClass[0]+' .'+nutClass[1]).val(vv);
							else
								$('#add-macronutrients tr.m'+(i+1)+' .'+nutClass[1]).val(vv);
						}

						//video id's
						$('#meal-'+(i+1)+ ' .video-id .youtube-id').val(v.youtube_id);
						$('#meal-'+(i+1)+ ' .video-id .vimeo-id').val(v.vimeo_id);
					});

					
					form.attr({  action : '/admin/workouts/update',
								'data-id' : id });
					form.find('input[name="name"]').val(v.wk_name);
					form.find('#description').code(v.wk_desc);
				});
			}
		});
	},
	reset : function(form){
		
		form.find('#main-wk-description input').each(function(){ $(this).val(''); });
		form.find('#main-wk-description #description').code('');
		form.find('#add-meals select').each(function(){ $(this).val('0|0|0'); });
		form.find('.help').each(function() { $(this).text('') });
		form.find('.cs-placeholder').each(function() { $(this).text('Please Select') });
		form.attr({  action : '/admin/workouts', 'data-id' : 0 });
	},

	delete: function(_this){

		_this = _this.closest('#alert-notification');

		data = {
			_token : $('form.workouts-action').find('input[name="_token"]').val(),
			slug_id: _this.attr('data-id'),
			table: _this.attr('data-table')
		}
		$.post('/admin/workouts/delete', data, function(response) {

			if(response.success) {
				
				$('.'+_this.attr('data-target-delete')).find('.panel-default').css({'border': '2px dashed #E33C3C'});
				setTimeout(function(){
					$('.'+_this.attr('data-target-delete')).fadeOut();
				}, 1000);
			}
			//console.log(response);
		})
	}	
};

//MEAL CALCULATION FORMULA
var mealCalculation = { 

	init: {},
	const: {
		TDC : 2000
	},
	macronutrients : function(_this) { 
	
		_mthis = this;
		_mthis.protein = {};_mthis.carbs = {};_mthis.fats = {};
		
		$.each(['tdc','m1','m2','m3','m4','m5','m6'], function(i, v){
			_mthis.protein[v] = _this.closest('form').find('#add-macronutrients .'+v+' input.p').val();
			_mthis.carbs[v]   = _this.closest('form').find('#add-macronutrients .'+v+' input.c').val();
			_mthis.fats[v]    = _this.closest('form').find('#add-macronutrients .'+v+' input.f').val();

		})
		return { protein : _mthis.protein, carbs : _mthis.carbs, fats : _mthis.fats };
	},

	meals : function() { 
	
		/*_mthis = this;
		_meal = {}; 
		//return this;
		
		$.each([1,2,3,4,5,6], function(i, v){

			_mthis['meal-'+v] = {}; //,'fat-calorie','carbohydrate-calorie'
			$.each(['protein-calorie'], function(ii, vv){
				
				_mthis['meal-'+v][vv] = {}; iii = 0
				$('#meal-'+v).find('.'+vv+' select').each(function(){
					
					_mthis['meal-'+v][vv][iii] = {};

						vaLue = $(this).val().split('|'); 
					_mthis['meal-'+v][vv][iii]['cal']  = vaLue[0];
					_mthis['meal-'+v][vv][iii]['unit'] = vaLue[2];
					
					iii++;
				});
			});
			_meal['meal-'+v] = _mthis['meal-'+v];
		});

		return _meal;*/

	},

	calculation : function(jthis){
		
		_this = this;
		_nutrients = _this.macronutrients(jthis);

		_thisval = jthis.closest('.form-group').find('select').val().split('|');
		_thisval = { calorie: _thisval[0], id: _thisval[1], unit: _thisval[2] };

		$.each([1,2,3,4,5,6], function(i,v){

			//M1 protein = [(TDC * (proTDC%/100)) * (proM1/100)] / valprocal 
			protein = ( [(_this.const.TDC) * (_nutrients.protein.tdc/100) * (_nutrients.protein["m"+v])/100 ] / _thisval.calorie ).toFixed(2); /// _thisval.calorie
			carbs 	= ( [(_this.const.TDC) * (_nutrients.carbs.tdc/100) * (_nutrients.carbs["m"+v])/100 ] / _thisval.calorie ).toFixed(2);
			fats 	= ( [(_this.const.TDC) * (_nutrients.fats.tdc/100) * (_nutrients.fats["m"+v])/100 ] / _thisval.calorie).toFixed(2);

			if(jthis.closest('.panel-collapse').attr('id') == 'meal-'+v) {
				if(jthis.closest('tr').attr('class') == 'protein-calorie') {
					jthis.closest('.form-group').find('.help').attr({
						'data-id' : _thisval.id,
						'data-unit': _thisval.unit,
						'data-value': protein
					}).text( protein+' '+_thisval.unit)
				}
				else if(jthis.closest('tr').attr('class') == 'carbohydrate-calorie') {
					jthis.closest('.form-group').find('.help').attr({
						'data-id' : _thisval.id,
						'data-unit': _thisval.unit,
						'data-value': carbs
					}).text( carbs+' '+_thisval.unit)
				}
				else if(jthis.closest('tr').attr('class') == 'fat-calorie') {
					jthis.closest('.form-group').find('.help').attr({
						'data-id' : _thisval.id,
						'data-unit': _thisval.unit,
						'data-value': fats
					}).text( fats+' '+_thisval.unit)
				}
			}
			//console.log( _nutrients.protein["m"+v] );
			//console.log(protein);
		});

		//console.log(_nutrients);
		//return _meal;
	} 
};
