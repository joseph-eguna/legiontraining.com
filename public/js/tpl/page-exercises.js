
//Show Modal For Adding New Exercises
$('body').on('click', '.modal-popup', function(){
	
	var _this = $(this);

	//$( _this.attr('data-target') ).find('form').attr( {'action' : '/admin/ingredients', 'data-id': 0} );
	
	//$( _this.attr('data-target') ).find('input.form-control').val('');
	
	//$( _this.attr('data-target') ).find('#description').code("");

	$( _this.attr('data-target') ).modal('show')

});

//Form on submit
$('body').on('submit', '.exercises-action', function(e){

	e.preventDefault();

	_this = $(this);

	formdata = {}; formdata['super_set'] = 0;
	$.each(_this.serializeArray(), function(i,v){

		if(['name', 'video-id','super_set',
			'set', 'repetitions', 'tempo_up','tempo_down',
			'short_of_failure', 'rest'].indexOf(v.name) != -1) {
			formdata[v.name] = v.value;
		}
	});
	
	data = {
		_token: _this.find('input[name="_token"]').val(),
		formdata : formdata
	};

	//console.log(data);

	$.post(_this.attr('action'), data, function(response){

		console.log(response);

		if(response.success){
			_this.find('.response').html('<p class="alert alert-success">Success!</p>')
			window.location.href="";
		} else {

				responseHtml = '<p class="alert alert-danger">';
			$.each(response, function(i,v){
				responseHtml += v + '<br />';
			});
				responseHtml += '</p>';

			_this.find('.response').html( responseHtml )
		}

		$('#modal-slide-up').animate({scrollTop:0}, '1000', 'swing');
	});
});


var exercises = {
	variable: {
		update : {
			url : '/admin/exercises/update',
			_token: $('.exercises-action').find('input[name="_token"]').val()
		},
		delete : {
			url : '/admin/exercises/delete',
			_token: $('.exercises-action').find('input[name="_token"]').val()
		}
	},
	trigger: function(){
		_this = this;
		//Updating exercises
		$('body').on('change', '.exercise input[type="checkbox"]', function(){

			_thisevent = $(this);

			//console.log($(this).attr('name'));
			//console.log( ($(this).is(':checked')) ? 'on' : 0 );
 
 			data = {
		    	_token : _this.variable.update._token,
		    	data_id: _thisevent.closest('.exercise').attr('data-id'),
		    	key: $(this).attr('name'),
		    	value: ($(this).is(':checked')) ? 'on' : 0
		   	}

		    $.post(_this.variable.update.url, data, function(response){

		    	//$('#alert-notification').modal('show')
		    	console.log(response);

		    });
		});

		//Updating exercises
		$('body').on('click', '.exercise .cs-wrapper', function(){

			_thisevent = $(this);

			//console.log($(this).attr('name'));
			//console.log( ($(this).is(':checked')) ? 'on' : 0 );
 
 			data = {
		    	_token : _this.variable.update._token,
		    	data_id: _thisevent.closest('.exercise').attr('data-id'),
		    	key: _thisevent.find('select').attr('name'),
		    	value: _thisevent.find('select').val()
		   	}

		    
		    $.post(_this.variable.update.url, data, function(response){

		    	console.log(response);

		    });

		});

		//show delete notification box
		$('body').on('click', '.delete', function(e){

			_thisevent = $(this);
			$('#alert-notification').modal('show').attr('data-id', _thisevent.closest('.exercise').attr('data-id'));
		});

		//Finally delete
		$('body').on('click', '#alert-notification input[type="submit"]', function(e){
			
			e.preventDefault();

			_thisevent = $(this);

			$.post(_this.variable.delete.url, { 
				data_id: _thisevent.closest('#alert-notification').attr('data-id'),
				_token: _this.variable.delete._token }, function(response){

					window.location.href = "";

			});
		});


		/*$('#alert-notification').on('show.bs.modal', function (e) {
		  	console.log( $(this) );
		})*/
	},
	responseHtml: function(){
		//
	}
};

exercises.trigger();


$(window).load(function(){

	//alert('Hello');
	setTimeout(function(){

		$('.video-display').each(function(){

			_thisevent = $(this);
			_id = _thisevent.attr('data-video-id');

			_thisevent.html( '<iframe src="http://player.vimeo.com/video/'+_id+'" frameborder="0" allowfullscreen="" style="width:100%"></iframe>' )
		
		});

	}, 3000);
		
});

		

