
//Show Modal For Adding New Ingredient
$('body').on('click', '.modal-popup', function(e){

	e.preventDefault();
	
	var _this = $(this);

	$( _this.attr('data-target') ).find('form').attr( {'action' : '/admin/ingredients', 'data-id': 0} );
	
	$( _this.attr('data-target') ).find('input.form-control').val('');
	
	$( _this.attr('data-target') ).find('#description').code("");

	
	$( _this.attr('data-target') ).modal('show')

});

//Modal Update
$('body').on('click', '.ingredients .fa.fa-edit', function(e){
	
	e.preventDefault();

	var _this = $(this);
		
	var data = {
		preview : _this.closest('.ingredient-box').find('.panel-body img').attr('src'),
		name 	: _this.closest('.ingredient-box').find('.panel-heading .panel-title').text(),
		unit 	: _this.closest('.ingredient-box').find('.panel-body .unit').text(),
		calorie : _this.closest('.ingredient-box').find('.panel-body .calories').text(),
		max_qty : _this.closest('.ingredient-box').find('.panel-body .max_qty').text(),
		description : _this.closest('.ingredient-box').find('.panel-body .desc').html()
	}

	$.each(data, function(i, v){
		
		if(i == 'preview' || i == 'description')
			$( _this.attr('data-target') ).find('.'+i).css('background', 'url('+v+')');
		else
			$( _this.attr('data-target') ).find('.'+i).val(v);

	});

	var categories = _this.closest('.ingredient-box').find('.categories').attr('data-categories');

	$.each($.parseJSON( categories ), function(i, v){

		$( _this.attr('data-target') ).find('#categories #ingredients-cbx-'+v).attr('checked', 'checked');

	});

	//$( _this.attr('data-target') ).find('#categories').each(function(){});



	$("#description").code(data.description);

	$( _this.attr('data-target') ).find('form').attr( {'action' : '/admin/ingredients/update', 'data-id': _this.attr('data-id')} );

	$( _this.attr('data-target') ).modal();

	//ingredient-box
	//dataForm = {}; 

});


//Delete Ingredient
$('body').on('click', '.ingredients .fa.fa-trash', function(e){
	
	e.preventDefault();

	var _this = $(this);

	dialog = confirm("Are you sure to delete?");


	if (dialog == true) {
		
		$.get('/admin/ingredients/delete', { id : _this.attr('data-id') }, function(response){
			
			_this.closest('.ingredient').remove();

			window.location.href = "";

		});

	} 
});

//Store and Update Ingredients
$('body').on('submit', '.ingredients-action', function(e){
	e.preventDefault();
	var _this = $(this);

	if(_this.attr('data-id') > 0) {
		ingredients.update($(this)); 
	} else {
		ingredients.store($(this)); 
	}
	

	/*var data = _this.serialize();
		data += '&description='+ $('#description').code();
		data += '&image_url='+_this.find('.preview').attr('data-url');
		data += '&id='+_this.attr('data-id');

		//data .push('image_url', _this.find('.preview').attr('data-url'));
		//console.log(data);

	_this.closest('.modal-dialog').find('.response *').remove();

	$.post(_this.attr('action'), data, function(response){
		
		console.log(response);
		if(response.message == 'success'){
			_this.closest('.modal-dialog').find('.response').append('<p class="alert alert-success"> Success! </p>');
			setTimeout(function(){
				window.location.href = "";
			}, 1000);
		} else {
			$.each(response, function(i, v){
				_this.closest('.modal-dialog').find('.response').append('<p class="alert alert-danger"> '+v+' </p>');
			});
		}
	});*/
});

var ingredients = {
	
		variable : {},

	store: function(_thisevent) {

		_this = this;
		
		data = _thisevent.serialize();
		data += '&description='+ $('#description').code();
		data += '&image_url='+_thisevent.find('.preview').attr('data-url');
		data += '&id='+_thisevent.attr('data-id');

		//console.log(data);
		$.post('/admin/ingredients', data, function(response){

			console.log(response);

			if(response.success)
				_this.responseHtml(_thisevent, ['Success'], true, true);
			else 
				_this.responseHtml(_thisevent, response, false, false);
			

		});

	},

	update: function(_thisevent) {

		_this = this;
		
		data = _thisevent.serialize();
		data += '&description='+ $('#description').code();
		data += '&image_url='+_thisevent.find('.preview').attr('data-url');
		data += '&id='+_thisevent.attr('data-id');

		//console.log(data);
		$.post('/admin/ingredients/update', data, function(response){

			console.log(response);

			if(response.success)
				_this.responseHtml(_thisevent, ['Success'], true, true);
			else 
				_this.responseHtml(_thisevent, response, false, false);
			

		});

	},

	responseHtml: function(_thisevent, response, success, refresh){

		_html = '';
		_class = 'class="alert alert-danger"';

		if(success){
			_class = 'class="alert alert-success"';
		} 

		//console.log(!$.isEmptyObject({t:'tes',r:123}));

		if(!$.isEmptyObject(response)) {
		
			$.each(response, function(i,v){
				_html +=  v + '<br />'
			});
			_html = '<p '+_class+'>'+_html+'</p>';

			_thisevent.closest('form').find('.response').html( _html );
		}

		if(refresh){
			window.location.href = ""; 
		} 

	}	

}


//Add Ingredients Category
$('body').on('blur', '#category-add', function(e){
	
	e.preventDefault();
	_this = $(this);
	
	var data = { 'category' : _this.val(),
				 '_token' : _this.closest('form').find('input[name="_token"]').val() }

	//console.log(_this.val());
	$.post(_this.attr('data-url'), data, function(response){
		if(response.message == 'success') {
			$('#categories').load( location.href + ' #categories');
			_this.val('');
		} 
	});
});

//Delete Ingredients Category
$('body').on('click', '#categories .fa-remove', function(e){
	
	e.preventDefault();
	_this = $(this);
	
	var data = { 'id' : _this.attr('data-id'),
				 '_token' : _this.closest('form').find('input[name="_token"]').val() }

	//console.log(_this.val());
	$.post( '/admin/ingredients/delete-category', data, function(response){

		if(response.message == 'success') {
			$('#categories').load( location.href + ' #categories');
		} 
	});
});

//Select Image
$('body').on('click','#select-image img', function(e){
	var _this = $(this);
	_this.closest('form').find('.preview').css('background', 'url('+_this.attr('src')+')');
	_this.closest('form').find('.preview').attr('data-url', _this.attr('src'));
	//.preview
});

//Delete Image
$('body').on('click','#select-image .fa-trash', function(e){
	
	var _this = $(this);
	
	var imgUrl = _this.parent().find('img').attr('data-src');

	var data = { 'img_url' : imgUrl,
				 '_token' : _this.closest('form').find('input[name="_token"]').val() }

	$.post(_this.attr('data-url'), data, function(response){
		 //On Success
		 _this.parent().remove();
	});
});

//Single Image upload
$('body').on('change', '#photo-upload', function(){
	
	var _this = $(this);
		
	//console.log(_this.closest('form').find('input[name="_token"]').val());
	var data = new FormData();
		data.append( 'photo', $('#photo-upload')[0].files[0] );
		data.append( '_token', _this.closest('form').find('input[name="_token"]').val() );

	$.ajax({
	   type: "POST",                
	   url: _this.attr('data-url'),
	   processData: false,
	   contentType: false,
	   cache:false,
	   data: data,
	   success: function(response){
	        _this.closest('form').find('.tabs').load(location.href + ' .tabs')
	    }
	});
});

