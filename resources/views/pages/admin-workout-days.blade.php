
@extends('layouts.admin-template')
@section('css')
    
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/select2.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/css/tpl/summernote.css" rel="stylesheet" type="text/css" media="screen">

    <!--link href="/css/tpl/dropzone.css" rel="stylesheet" type="text/css" media="screen"-->
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/admin.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
         <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
@endsection

@section('js')
<div class="jsplace">
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    <!--script src="/js/tpl/dropzone.min.js"></script-->
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>

    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/select2.min.js" type="text/javascript" ></script>

    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script>
    
    <!--script src="/js/tpl/classie.js" type="text/javascript" ></script>   
    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script-->

    <!--script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>
    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script-->
    <!--script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script-->
    
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>  
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/page-workout-day.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#description, #email-content').summernote({
                height:150,
                toolbar: [
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['color', ['color']]
                         ],
                placeholder: 'Enter Full Description'
            });
        });

    </script>
</div>
@endsection

@section('content')
<div class="page-content-wrapper ">
    <div class="padding-25"></div>
    <div class="content">
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                

                <div class="col-md-9">


                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                        
                            <!--button class="btn btn-green btn-lg pull-right bottom-right" id="btnToggleSlideUpSize">Generate</button-->


                            <a href="#" class="btn btn-default pull-right modal-popup add-workouts" data-target="#modal-slide-up" >ADD NEW DAY</a>
                            <div class="panel-title">
                                <h3 class="text-uppercase">{{ $name }} </h3>
                            </div>
                        </div>
                        


            <div class="row workouts">
            @if(count($workout_days) > 0)
                <?php $i = count($workout_days); ?>
                @foreach($workout_days as $workout_day)
                <div class="col-md-4 workout-day day-{{$workout_day->id}}">
                    <div class="panel panel-default" data-id="{{$workout_day->id}}">
                        <div class="panel-heading ">
                            <div class="panel-title">
                               Day {{ $i }} - {{ $workout_day->name }}</a>
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li>
                                        <a class="fa fa-edit modal-popup" data-id="{{ $workout_day->id }}" data-target="#modal-slide-up"  
                                        data-toggle="tooltip" 
                                        title="Edit - {{ $workout_day->name }}" href="#"></a>
                                    </li>
                                    <li>
                                        <a class="fa fa-trash delete-notify wk_day" data-target="#alert-notification" 
                                        data-toggle="tooltip" 
                                        title="Edit - {{ $workout_day->name }}"
                                        data-id="{{ $workout_day->id }}" href=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body" style="display: block;">
                            <!--img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif" class="img-responsive"/-->
                            <img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif"
                                 data-src="http://www.bodybuildingestore.com/wp-content/uploads/2012/06/Workouts-of-Advanced-Bodybuilders.jpg" 
                            class="img-responsive lazy-loading" />
                            <hr>
                            <div class="description">
                                {!! $workout_day->description !!}
                            </div>
                        </div>
                    </div>
                </div>  
                <?php $i--; ?>
                @endforeach
            @else
                <h1>{ Empty }</h1>
            @endif
            </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-right" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
                    <h5 class="text-uppercase">Add New Day</h5>
                    <div class="response"></div>
                </div>
                <div class="modal-body">

                    <form class="workouts-action" role="form" method="POST" action="">
                       
                        {{ csrf_field() }}
                        <div class="response"></div>
                        <div class="panel panel-transparent tabs">
                            <ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#main-description"><span>Main</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#exercises"><span>Exercises</span></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="main-description">
                                    <div class="row">    
                                        <div class="form-group">
                                            <label>Name:</label>
                                            <input type="text" placeholder="Enter workout Name:" name="name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Full Description:</label>
                                            <div class="summernote-wrapper">
                                                <div id="description" name="description"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Email Subject:</label>
                                            <input type="text" placeholder="Enter Email Subject:" name="email_subject" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Email Content:</label>
                                            <div class="summernote-wrapper">
                                                <div id="email-content" name="email_content" ></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="exercises">
                                    <div class="row">



<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">SELECT EXERCISES
            <span class="text-danger" style="cursor:pointer;"
                  data-placement="right"
                  data-toggle="popover" data-content="<p style='text-transform:none;'>
                        Select the workouts you wanted for this day and click Add. Then, this will be reloaded on your
                        workout list below.</p>"> (info) </span>
        </div>
    </div>
    <div class="panel-body">
        <div class="col-md-9 select-exercise">
            <select class="full-width" data-init-plugin="select2">
                 @if(count($exercises) > 0)
                    @foreach($exercises as $exercise)
                        <option value="{{ $exercise->id }}">{{ $exercise->name }}</option>
                    @endforeach
                @endif
                <!--optgroup label="Alaskan/Hawaiian Time Zone">
                    <option value="AK">Alaska</option>
                    <option value="HI">Hawaii</option>
                </optgroup-->
            </select>
        </div>
        <div class="col-md-3">
            <button class="form-control btn btn-primary" id="add-exercise" data-target="select-exercise">ADD</button>
        </div>
        <div class="clearfix"></div>

    </div>
    <!--div class="panel-heading">
        <div class="panel-title">
            <span class="text-danger" style="cursor:pointer;"
                  data-placement="right"
                  data-toggle="popover" data-content="<p style='text-transform:none;'>
                        Select the workouts you wanted for this day and click Add. Then, this will be reloaded on your
                        workout list below.</p>"> (info) </span>
        </div>
    </div-->
</div>

<div class="panel panel-default list">
    <div class="panel-heading">
        <div class="panel-title">LIST OF EXERCISES 
            <span class="text-danger" style="cursor:pointer;"
                  data-toggle="popover" 
                  data-content="<p style='text-transform:none;'>This will show the list of exercises on this day.
                                There are defaults settings into it and those are coming here from workout page.</p>"> (info) </span></div>
    </div>
    <div class="panel-body" id="day-exercise-list">
        <div class="panel-group" aria-multiselectable="true">
            
        @if(count($day_exercises) > 0)
            <?php $i=1; ?>
            @foreach($day_exercises as $day_exercise)
            
            <div class="panel panel-default" data-id="{{ $day_exercise->id }}">
                <div class="panel-heading" role="tab">
                    <a class="fa fa-trash text-danger pull-left delete-exercise" style="font-size: 20px;color: #F31403 !important;position: absolute;cursor:pointer;left: 5px;top`: 10px;top: -5px;"></a>
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#day-exercise-{{$day_exercise->id}}" aria-expanded="true" aria-controls="collapseOne">
                            {{ $day_exercise->name }}
                        </a>
                    </h4>
                </div>
                <?php $class_in = (count($day_exercises) == $i) ? 'in' : ''; ?>
                <div id="day-exercise-{{$day_exercise->id}}" class="panel-collapse collapse {{ $class_in }}" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                            
                            <!--img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif" class="img-responsive"/-->
                        <iframe src="http://player.vimeo.com/video/{{ $day_exercise->video_id }}" frameborder="0" allowfullscreen="" style="width:100%"></iframe>

                        <!--div class="video-display" data-video-id=""></div-->
                        
                        <hr />

                        <?php $day_exercise->super_set = (!$day_exercise->super_set) ? '' : 'checked="checked"' ; ?>
                        <p><strong>SUPER SET: </strong> <input type="checkbox" class="checkbox" name="super_set" data-init-plugin="switchery" {!! $day_exercise->super_set !!} /></p>

                        <p><strong>SET:  </strong> 
                            <select class="cs-select cs-skin-slide" name="set" data-init-plugin="cs-select">
                                <option>{{ $day_exercise->set }}</option>
                                @for($n=1;11>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>REPETITIONS: </strong> 
                            <select class="cs-select cs-skin-slide" name="repetitions" data-init-plugin="cs-select">
                                 <option>{{ $day_exercise->repetitions }}</option>
                                 @for($n=1;101>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>TEMPO: </strong><br />
                            <select class="cs-select cs-skin-slide" name="tempo_up" data-init-plugin="cs-select">
                               <option value="{{ $day_exercise->tempo_up }}">{{ $day_exercise->tempo_up }} up</option>
                                @for($n=1;11>$n;$n++)
                                    <option value="{{$n}}">{{$n}} up</option>
                                @endfor
                            </select> / 
                            <select class="cs-select cs-skin-slide" name="tempo_down" data-init-plugin="cs-select">
                                <option value="{{ $day_exercise->tempo_down }}">{{ $day_exercise->tempo_down }} down</option>
                                @for($n=1;11>$n;$n++)
                                    <option value="{{$n}}">{{$n}} down</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>SHORT OF FAILURE:</strong> 
                            <select class="cs-select cs-skin-slide" name="short_of_failure" data-init-plugin="cs-select">
                                <option>{{ $day_exercise->short_of_failure }}</option>
                                @for($n=1;11>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>REST: </strong> 
                            <select class="cs-select cs-skin-slide" name="rest" data-init-plugin="cs-select">
                                <option>{{ $day_exercise->rest }}</option>
                                @for($n=1;241>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                            
                    </div>
                </div>
            </div>
            <?php $i++; ?>
            @endforeach
        @endif

        </div>
    </div>
</div>




                                    </div>
                                </div>
                                <hr />
                                <div class="col-md-4 pull-right">
                                    <input type="submit" class="form-control btn btn-primary" value="Save" />  
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>

                                

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="alert-notification" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix"></div>
                <div class="modal-body">
                    <h6 class="text-center text-uppercase">Are you sure you want to delete?</h6>
                    <div class="form-group">
                        <a href="#"  data-dismiss="modal" class="btn btn-info">CANCEL</a>
                        <input type="submit" data-dismiss="modal" class="pull-right btn btn-danger" value="DELETE">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

