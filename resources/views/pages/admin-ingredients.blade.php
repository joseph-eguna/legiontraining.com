
@extends('layouts.admin-template')
@section('css')
    
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    
    <link href="/css/tpl/summernote.css" rel="stylesheet" type="text/css" media="screen">

    <!--link href="/css/tpl/dropzone.css" rel="stylesheet" type="text/css" media="screen"-->
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/admin.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
         <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
@endsection

@section('js')
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    <!--script src="/js/tpl/dropzone.min.js"></script-->
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script>

    <!--script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>

    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>
    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script-->
    
    <script type="text/javascript">
        $(document).ready(function(){
            $('#description').summernote({
                height:150,
                toolbar: [
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['color', ['color']]
                         ],
                placeholder: 'Enter Full Description'
            });
        });  
    </script>

    <script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/page-ingredients.js"></script>
@endsection

@section('content')
<div class="page-content-wrapper ">
    <div class="padding-25"></div>
    <div class="content">
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                

                <div class="col-md-9">


                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                        
                            <!--button class="btn btn-green btn-lg pull-right bottom-right" id="btnToggleSlideUpSize">Generate</button-->


                            <a href="#" class="btn btn-default pull-right modal-popup add-ingredients" data-target="#modal-slide-up" >ADD INGREDIENTS</a>
                            <div class="panel-title">
                                <h3>Ingredients</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row ingredients">

                                @if(count($ingredients) > 0)
                                    <?php $i=1;?>
                                    @foreach($ingredients as $ingredient)
                                    <?php $ingredient_categories = json_encode(unserialize($ingredient->categories)); ?>
                                    <div class="col-md-4 ingredient">
                                        <div data-pages="portlet" class="panel panel-default ingredient-box" id="portlet-basicx">
                                            <div class="panel-heading ">
                                                <div class="panel-title"> {{ $ingredient->name }}</div>
                                                <div class="panel-controls">
                                                    <ul>
                                                        <!--li><a class="portlet-collapse" href="#"><i class="pg-arrow_maximize"></i></a> </li-->
                                                        <li><a class="fa fa-edit" data-id="{{ $ingredient->id }}" data-target="#modal-slide-up" data-id="{{ $ingredient->id }}" href="#"></a></li>
                                                        <li><a class="fa fa-trash" data-id="{{ $ingredient->id }}" href="#"></a> </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="display: block;"> 
                                                
                                                @if($ingredient->image_url)
                                                    <img src="{{ $ingredient->image_url }}" class="img-responsive">
                                                @else
                                                    <img src="http://placehold.it/200x100" class="img-responsive" />
                                                @endif
                                                
                                                <hr>
                                                <span class="categories" data-categories='{!! $ingredient_categories !!}'></span>

                                                <p><strong>UNIT: </strong> <span class="unit">{{ $ingredient->unit }}</span></p>
                                                <p><strong>CALORIES: </strong> <span class="calories"> {{ $ingredient->calories }}</span> </p>
                                                <p><strong>MAX QTY: </strong> <span class="max_qty">{{ $ingredient->max_quantity }}</span></p>
                                                <p><strong>DESCRIPTION:</strong></p>
                                                <div class="desc">
                                                    {!! $ingredient->description !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @if($i%3==0)
                                        <div class="clearfix"></div>
                                    @endif
                                    <?php $i++; ?>
                                    @endforeach

                                @else
                                    <h1>{ Empty }</h1>
                                @endif

                                <!--div class="col-md-4">
                                    <div data-pages="portlet" class="panel panel-default" id="portlet-basic">
                                        <div class="panel-heading ">
                                            <div class="panel-title"> Rice Cakes </div>
                                            <div class="panel-controls">
                                                <ul>
                                                    <li><a data-toggle="collapse" class="portlet-collapse" href="#"><i class="pg-arrow_maximize"></i></a> </li>
                                                    <li><a data-toggle="refresh" class="portlet-edit" href="#"><i class="fa fa-edit"></i></a> </li>
                                                    <li><a data-toggle="close" class="portlet-trash" href="#"><i class="fa fa-trash"></i></a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel-body" style="display: block;"> <img src="http://legiontraining.com/members/wp-content/uploads/sites/6/2014/02/rice-cakes-574x270.jpg" class="img-responsive">
                                            <hr>
                                            <p><strong>UNIT: </strong> grams</p>
                                            <p><strong>CALORIES: </strong> 50</p>
                                            <p><strong>MAX QTY: </strong> N/A</p>
                                            <p><strong>DESCRIPTION:</strong>
                                                <br>Basic Portlet tools include a slide toggle button All these are fully customizable and come with callback functions to integrate with your code. Clicking on the refresh button will simulate an AJAX call. </p>
                                        </div>
                                    </div>
                                </div-->
                                
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- ACTION FORM -->
<div class="modal fade slide-up" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
                    <h5>INGREDIENTS</h5>
                    <div class="response"></div>
                </div>
                <div class="modal-body">

                    <!--form action="/admin/ingredients/upload" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="file" id="photo-uploadx" data-url="/admin/ingredients/upload" name="photo" class="form-control">
                        <input type="submit">
                    </form-->
                    <form class="row ingredients-action" role="form" method="POST" action="{{ url('/admin/ingredients') }}">
                       
                        {{ csrf_field() }}

                        <div class="response"></div>
                        <!--input type="hidden" name="image_url" value="/storage/app/ingredients/1472177787"-->
                        <div class="panel panel-transparent tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#select-image"><span>Select Image</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#upload-image"><span>Upload Image</span></a>
                                </li>

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style="background:#3F3F3F;margin:0 -13px">
                                <div class="tab-pane active" id="select-image">
                                    <div class="row column-seperation">
                                        
                                        <div class="col-md-12">
                                            @if($images)
                                                @foreach($images as $img_url)
                                                <?php static $i=0; ?>
                                                <div class="col-md-3" style="padding: 5px;">
                                                    <img data-src="{{ $img_url }}" src="/storage/app/{{ $img_url }}" class="img-responsive" style="max-width:100%;">
                                                    <span class=" text-danger fa fa-trash" data-url="/admin/ingredients/delete"></span>
                                                </div>
                                                
                                                @if($i%4 == 0)
                                                    <div class="clearfix"></div>
                                                @endif
                                                <?php $i++; ?> 
                                                @endforeach
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane" id="upload-image">
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="form-group form-group-default bg-transparent" style="border:transparent;">
                                                <input type="file" id="photo-upload" data-url="/admin/ingredients/upload" name="photo" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group-attached">
                            <div class="col-sm-12">
                                <div class="col-md-4 row">
                                    <!--div class="form-group form-group-default">
                                        <input type="file" id="photo-upload" data-url="/admin/ingredients/upload" name="photo" class="form-control">
                                    </div-->
                                    <div class="form-group">
                                        <div class="preview" data-url="" style="background: url(http://placehold.it/150x100);"></div>
                                        <hr />
                                        <label class="text-center">Categories:</label>
                                        <input type="text" id="category-add" data-url="/admin/ingredients/category" name="category" class="form-control"> </div>
                                    <div id="categories">
                                        @foreach($categories as $cat)
                                        <div class="checkbox check-success">
                                            <input type="checkbox" name="categories[]" value="{{ $cat->id }}" id="ingredients-cbx-{{ $cat->id }}">
                                            <label for="ingredients-cbx-{{ $cat->id }}">{{ $cat->name }}</label>
                                            <span class="fa fa-remove" data-id="{{ $cat->id }}" style="cursor:pointer;"></span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-sm-8 row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Ingredients Name:</label>
                                            <input type="text" name="name" class="name form-control"> 
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Unit:</label>
                                            <select name="unit" class="unit form-control">
                                                @foreach($units as $unit)
                                                    <option>{{ $unit }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Calories:</label>
                                            <input type="text" name="calorie" class="calorie form-control"> </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Max Qty:</label>
                                            <input type="text" name="max_qty" class="max_qty form-control"> </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="form-group">
                                            <label class="">Description:</label>
                                            <!--textarea name="description" rows="10" class="form-control"></textarea-->
                                            <div class="summernote-wrapper">
                                                <div id="description"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 row">
                                        <div class="col-sm-8">
                                            <div class="p-t-20 clearfix p-l-10 p-r-10"> </div>
                                        </div>
                                        <div class="col-md-4 m-t-10 sm-m-t-10">
                                            <button type="submit" class="btn btn-primary btn-block m-t-5">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    
                         </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

