
@extends('layouts.admin-template')
@section('css')
    
    <link href="/css/tpl/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/c3.min.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/css/tpl/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
         <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
@endsection

@section('content')

<div class="page-content-wrapper ">
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">

        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>
    <!--script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>

    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>

    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script-->
    <script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/js/tpl/pages.min.js"></script>

    <script src="/js/tpl/scripts.js" type="text/javascript"></script>
@endsection