
@extends('layouts.members-template')
@section('css')
	
    <link href="/css/tpl/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/c3.min.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/css/tpl/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
	     <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	  <![endif]-->
@endsection

@section('content')

<div class="page-content-wrapper ">
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">

	<div class="row">
        <div class="col-md-4 pull-right chart-positions" style="position:fixed;right:0;bottom: -20px;height: calc(100vh - 65px);overflow-y: scroll;">

            <div class="row">
                <div class="panel text-center" style="background: rgba(255, 255, 255, 0.12) !Important;">
                    <!--div class="panel-heading">
                        <div class="panel-title">Exercise Progress in Minutes</div>
                    </div>
                    <div class="panel-body">
                         <div id="area-chart"></div>
                    </div-->

                    <div class="panel-heading">
                        <div class="panel-title text-white">Total Calories Reduced</div>
                    </div>
                    <div class="panel-body">
                         <div id="donut-chart"></div>
                    </div>

                    <div class="panel-heading">
                        <div class="panel-title text-white">Daily Average Workout</div>
                    </div>
                    <div class="panel-body">
                        <div id="guage-chart"></div>
                    </div>
                </div>
                
                

            </div>

            <!--div class="panel no-border  no-margin">
                <div class="panel-heading">
                    <div class="panel-title">Exercise Progress in Minutes</div>
                    <div class="panel-controls">
                        <ul>
                            <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i
class="portlet-icon portlet-icon-refresh"></i></a> </li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="area-chart"></div>
                    <div class="clear:both"></div>
                </div>
                
            </div-->
            <div class="clear:both"></div>
        </div>
        <div class="col-md-8">
            <div class="widget-16 panel no-border  no-margin widget-loader-circle bg-transparent">
                <div class="panel-heading text-center">
                    <div class="panel-title text-white">View Members Progress</div>
                </div>
                <div class="panel-body">
                    <div class="timeline-container top-circle">
                        <section class="timeline">
                            <div class="timeline-block">
                                <div class="timeline-point success"> <i class="pg-map"></i> </div>
                                <!-- timeline-point -->
                                <div class="timeline-content">
                                    <div class="card share full-width">
                                        <div class="circle" data-toggle="tooltip" title="Label" data-container="body"> </div>
                                        <div class="card-header clearfix">
                                            <div class="user-pic"> <img alt="Profile Image" width="33" height="33" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg"> </div>
                                            <h5>Jeff Curtis</h5>
                                            <h6>Shared a Tweet
                    <span class="location semi-bold"><i class="fa fa-map-marker"></i> SF, California</span>
                </h6> </div>
                                        <div class="card-description">
                                            <p>What you think, you become. What you feel, you attract. What you imagine, you create - Buddha. <a href="#">#quote</a> </p>
                                            <div class="via">via Twitter</div>
                                        </div>
                                    </div>
                                    <!--div class="event-date">
                                        <h6 class="font-montserrat all-caps hint-text m-t-0">Apple Inc</h6> <small class="fs-12 hint-text">15 January 2015, 06:50 PM</small></div-->
                                </div>
                                <!-- timeline-content -->
                            </div>
                            <!-- timeline-block -->
                            <div class="timeline-block">
                                <div class="timeline-point small"> </div>
                                <!-- timeline-point -->
                                <div class="timeline-content">
                                    <div class="card share full-width grey">
                                        <div class="circle" data-toggle="tooltip" title="Label" data-container="body"> </div>
                                        <div class="card-header clearfix">
                                            <div class="user-pic"> <img alt="Profile Image" width="33" height="33" data-src-retina="assets/img/profiles/5x.jpg" data-src="assets/img/profiles/5.jpg" src="assets/img/profiles/5x.jpg"> </div>
                                            <h5>Shannon Williams</h5>
                                            <h6>Shared a photo
                    <span class="location semi-bold"><i class="fa fa-map-marker"></i> NYC, New York</span>
                </h6> </div>
                                        <div class="card-description">
                                            <p>Inspired by : good design is obvious, great design is transparent</p>
                                            <div class="via">via themeforest</div>
                                        </div>
                                        <!--div class="card-footer clearfix">
                                            <div class="time">few seconds ago</div>
                                            <ul class="reactions">
                                                <li><a href="#">5,345 <i class="fa fa-comment-o"></i></a> </li>
                                                <li><a href="#">23K <i class="fa fa-heart-o"></i></a> </li>
                                            </ul>
                                        </div-->
                                    </div>
                                    <!--div class="event-date"> <small class="fs-12 hint-text">15 January 2015, 06:50 PM</small> </div-->
                                </div>
                                <!-- timeline-content -->
                            </div>
                            <div class="timeline-block">
                                <div class="timeline-point small"> </div>
                                <!-- timeline-point -->
                                <div class="timeline-content">
                                    <div class="card share full-width">
                                        <div class="circle" data-toggle="tooltip" title="Label" data-container="body"> </div>
                                        <div class="card-header clearfix">
                                            <div class="user-pic"> <img alt="Profile Image" width="33" height="33" data-src-retina="assets/img/profiles/4x.jpg" data-src="assets/img/profiles/4.jpg" src="assets/img/profiles/4x.jpg"> </div>
                                            <h5>Andy Young</h5>
                                            <h6>Updated his status
                    <span class="location semi-bold"><i class="icon-map"></i> NYC, New York</span>
                </h6> </div>
                                        <div class="card-description">
                                            <p>What a lovely day! I think I should go and play outside.</p>
                                        </div>
                                    </div>
                                    <!--div class="event-date"> <small class="fs-12 hint-text">15 January 2015, 06:50 PM</small> </div-->
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </div>

            </div>
        <!--div class="container-fluid container-fixed-lg footer">
            <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset"> <span class="hint-text">Copyright &copy; 2014 </span> <span class="font-montserrat">REVOX</span>. <span class="hint-text">All rights reserved. </span> <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span> </p>
                <p class="small no-margin pull-right sm-pull-reset"> <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span> </p>
                <div class="clearfix"></div>
            </div>
        </div-->
    </div>

</div>

@endsection

@section('js')
    <script src="/js/tpl/pace.min.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    
    <!--script src="/js/tpl/jquery-easy.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.bez.min.js"></script>
    <script src="/js/tpl/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.actual.min.js"></script-->
    
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>
    <script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>
    
    <script src="/js/tpl/d3.min.js"></script>
    <script src="/js/tpl/c3.min.js"></script>

    <!--script src="/js/tpl/interactiveLayer.js" type="text/javascript"></script>
    <script src="/js/tpl/axis.js" type="text/javascript"></script>
    <script src="/js/tpl/line.js" type="text/javascript"></script>
    <script src="/js/tpl/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="/js/tpl/hammer.js"></script>
    <script src="/js/tpl/jquery.mousewheel.js"></script>
    <script src="/js/tpl/mapplic.js"></script-->

    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>

    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/js-charts.js" type="text/javascript"></script>
    <!--script src="/js/tpl/dashboard.js" type="text/javascript"></script-->


<!--script type="text/javascript">
    $(function() {
        $("#sparkline-line").sparkline([0, 20, 25, 30, 40, 50, 50, 60], {
            type: 'line',
            width: $(this).width(),
            height: '200',
            fillColor: 'rgba(15, 130, 120, 0.7)', // Get Pages contextual color
            lineColor: 'rgba(0,0,0,0)',
            highlightLineColor: 'rgba(0,0,0,.09)',
            highlightSpotColor: 'rgba(0,0,0,.21)',
		});

		$("#sparkline-line").sparkline([0, 10, 20, 35, 20, 30, 40, 40], {
            type: 'line',
            width: $(this).width(),
            height: '200',
            fillColor: 'rgba(241, 183, 11, 0.51)', // Get Pages contextual color
            lineColor: 'rgba(0,0,0,0)',
            highlightLineColor: 'rgba(0,0,0,.09)',
            highlightSpotColor: 'rgba(0,0,0,.21)',
            composite: true
		});

    });
</script-->
    

    <!--script type="text/javascript">

        var chart2 = c3.generate({
            bindto: '#area-chart',
            color: {
              pattern: [ '#FFBC0B', '#F5393D', '#4FD8B0', '#67D3E0', '#C3D62D', '#5A5386', '#FF7022']
            },
            padding: {
            left: 30,
              right: 15,
              top: 0,
              bottom: 0
           },
            data: {
                columns: [
                    ['GOAL',        35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35, 35],
                    ['MY PROGRESS', 35, 37, 33, 32, 35,36]
                ],
                types: {
                    data1: 'area',
                    data2: 'area-spline'
                }
            }
        });

    </script-->


    <script src="/js/tpl/scripts.js" type="text/javascript"></script>
@endsection