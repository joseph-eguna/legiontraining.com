
@extends('layouts.admin')


@section('left-sidebar')
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                
                <li>
                    <a href="#">
                        <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" class="img-responsive" />
                    </a>
                </li>
                <li>
                    <a href="#"><i class="icon-categories"></i> Overview</a>
                </li>
                <li style="position:static;">
                    <a href="#"><i class="icon-barbell"></i> Legion Workout <span class="fa arrow"></span></a>
                   <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Exercise</a>
                        </li>
                        <li>
                            <a href="#">Boulders for Shoulders Workout</a>
                        </li>
                        <li>
                            <a href="#">Ultimate Shred Workouts</a>
                        </li>
                        <li>
                            <a href="#">Pre-Conditioning Workouts</a>
                        </li>
                        <li>
                            <a href="#"> Male Workout</a>
                        </li>
                        <li>
                            <a href="#">Female Workout</a>
                        </li>
                        <li>
                            <a href="#">Hell Legs Workout</a>
                        </li>
                        <li>
                            <a href="#">Pure Bulk Workouts</a>
                        </li>
                        <li>
                            <a href="#">Back Attack</a>
                        </li>
                        <li>
                            <a href="#">Legion on The Hustle</a>
                        </li>
                        <li>
                            <a href="#">Legion Ignite</a>
                        </li>
                        <li>
                            <a href="#">Rack Attack</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-apple"></i> Legion Meals <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#">Meal Plan</a>
                        </li>
                        <li>
                            <a href="#">Ingredients</a>
                        </li>
                    </ul>
                </li>
                

               
                <li>
                    <a href="#"><i class="icon-wallet"></i> Resources<span class="fa arrow"></span></a>
                    
                </li>
                <li>
                    <a href="#"><i class="fa fa-files-o fa-fw"></i> Legion Programs<span class="fa arrow"></span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-comment-1"></i> Support<span class="fa arrow"></span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-facebook-square"></i> Social<span class="fa arrow"></span></a>
                </li>
            </ul>
        </div>
    </div>    
@endsection

@section('content')
<div class="row dash-profile" style="padding-top: 30px;">
    <div class="col-md-9">
        <?php #var_dump($units); ?>

        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-uppercase">Add New</h3> </div>
            <div class="panel-body">
                
                <form class="row" role="form" method="POST" action="{{ url('/admin/ingredients/add') }}">
                    
                    {{ csrf_field() }}

                    <div class="col-md-4">
                    
                        <div class="form-group">
                            <label>Image</label> <img src="http://legiontraining.com/members/wp-content/uploads/sites/6/2014/02/rice-cakes-574x270.jpg" class="img-responsive"> </div>
                        <div class="form-group">
                            <label>Unit:</label>
                            <select class="form-control" name="unit">
                                @foreach($units as $unit)
                                    <option>{{ $unit }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Calories:</label>
                            <input class="form-control" name="calories"> 
                        </div>
                        <div class="form-group">
                            <label>Max Quantity:</label>
                            <input class="form-control" name="max_qty">
                        </div>
                        <div class="form-group">
                            <label>Categories:</label>
                            <a href="#more-category" class="btn btn-default add-category-button">ADD CATEGORIES </a>
                            <input id="more-category" class="form-control" name="add_category">
                        </div>
                    
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Name: </label>
                            <input class="form-control" name="name"> </div>
                            
                            @if ($errors->has('name'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif

                        <div class="form-group">
                            <label>Description:</label>
                            <textarea name="description" name="description" class="form-control" rows="7"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Substitute:</label>
                            <select multiple="" name="substitute[]" class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

