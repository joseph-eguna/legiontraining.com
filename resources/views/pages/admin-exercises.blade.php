
@extends('layouts.admin-template')
@section('css')
    
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    
    <link href="/css/tpl/summernote.css" rel="stylesheet" type="text/css" media="screen">

    <!--link href="/css/tpl/dropzone.css" rel="stylesheet" type="text/css" media="screen"-->
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/admin.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
         <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
@endsection

@section('js')
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min-2.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    <!--script src="/js/tpl/dropzone.min.js"></script-->
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>

    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>

    <!--script src="/js/tpl/classie.js" type="text/javascript" ></script>   
    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script-->

    <!--script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>
    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script-->
    <!--script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script-->
    
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>  
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/page-exercises.js"></script>

    <script type="text/javascript">
        /*$(document).ready(function(){
            $('#description').summernote({
                height:150,
                toolbar: [
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['color', ['color']]
                         ],
                placeholder: 'Enter Full Description'
            });
        });  */
    </script>

@endsection

@section('content')
<div class="page-content-wrapper ">
    <div class="padding-25"></div>
    <div class="content">
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                

                <div class="col-md-9">


                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                        
                            <!--button class="btn btn-green btn-lg pull-right bottom-right" id="btnToggleSlideUpSize">Generate</button-->


                            <a href="#" class="btn btn-default pull-right modal-popup add-exercises" data-target="#modal-slide-up" >ADD EXERCISES</a>
                            <div class="panel-title">
                                <h3>EXERCISES</h3>
                            </div>
                        </div>
                        




<div class="row exercises">
@if(count($exercises) > 0)
    @foreach($exercises as $exercise)
    <?php static $i = 0;?>
    <div class="col-md-4 exercise" data-id="{{ $exercise->id }}">
        <div data-pages="portlet" class="panel panel-default" id="portlet-basic">
            <div class="panel-heading ">
                <div class="panel-title">{{ $exercise->name }}</div>
                <div class="panel-controls">
                    <ul>
                        <li>
                            <a class="fa fa-trash delete" href="#"></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel-body" style="display: block;">
                <div class="video-display" data-video-id="{{ $exercise->video_id }}">
                    <img src="https://d13yacurqjgara.cloudfront.net/users/12755/screenshots/1037374/hex-loader2.gif" class="img-responsive"/>
                    <!--iframe src="http://player.vimeo.com/video/153755097" frameborder="0" allowfullscreen="" style="width:100%"></iframe-->

                    <!--img src="https://cdn.muscleandstrength.com/sites/default/files/styles/400x250/public/field/image/workout/hiit-fat-loss-thumbnail.jpg?itok=M1uv2uY4" class="img-responsive"-->
                </div>
                <hr>
                
                <?php $exercise->super_set = (!$exercise->super_set) ? '' : 'checked="checked"' ; ?>
                <p><strong>SUPER SET: </strong> <input type="checkbox" class="checkbox" name="super_set" data-init-plugin="switchery" {!! $exercise->super_set !!} /></p>

                <p><strong>SET:  </strong> 
                    <select class="cs-select cs-skin-slide" name="set" data-init-plugin="cs-select">
                        <option>{{ $exercise->set }}</option>
                        @for($n=1;11>$n;$n++)
                            <option>{{$n}}</option>
                        @endfor
                    </select>
                </p>
                <p><strong>REPETITIONS: </strong> 
                    <select class="cs-select cs-skin-slide" name="repetitions" data-init-plugin="cs-select">
                         <option>{{ $exercise->repetitions }}</option>
                         @for($n=1;101>$n;$n++)
                            <option>{{$n}}</option>
                        @endfor
                    </select>
                </p>
                <p><strong>TEMPO: </strong><br />
                    <select class="cs-select cs-skin-slide" name="tempo_up" data-init-plugin="cs-select">
                       <option value="{{ $exercise->tempo_up }}">{{ $exercise->tempo_up }} up</option>
                        @for($n=1;11>$n;$n++)
                            <option value="{{$n}}">{{$n}} up</option>
                        @endfor
                    </select> / 
                    <select class="cs-select cs-skin-slide" name="tempo_down" data-init-plugin="cs-select">
                        <option value="{{ $exercise->tempo_down }}">{{ $exercise->tempo_down }} down</option>
                        @for($n=1;11>$n;$n++)
                            <option value="{{$n}}">{{$n}} down</option>
                        @endfor
                    </select>
                </p>
                <p><strong>SHORT OF FAILURE:</strong> 
                    <select class="cs-select cs-skin-slide" name="short_of_failure" data-init-plugin="cs-select">
                        <option>{{ $exercise->short_of_failure }}</option>
                        @for($n=1;11>$n;$n++)
                            <option>{{$n}}</option>
                        @endfor
                    </select>
                </p>
                <p><strong>REST: </strong> 
                    <select class="cs-select cs-skin-slide" name="rest" data-init-plugin="cs-select">
                        <option>{{ $exercise->rest }}</option>
                        @for($n=1;241>$n;$n++)
                            <option>{{$n}}</option>
                        @endfor
                    </select>
                </p>

            </div>
        </div>
    </div> 
    <?php $i++; ?>
    @if($i%3 == 0)
        <div class="clearfix"></div>
    @endif
    @endforeach
@else
    <h1 class="col-md-12">{ Empty }</h1>
@endif
</div>




                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-right in" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
                    <h5>EXERCISE</h5>
                    <div class="response"></div>
                </div>
                <div class="modal-body">

                    <form class="exercises-action" role="form" method="POST" action="{{ url('/admin/exercises') }}">
                       
                        <div class="response"></div>
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" name="name" placeholder="Enter Exercise Name:" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Video ID:</label>
                            <input type="text" name="video-id" placeholder="YOUTUBE ID:"  class="form-control">
                        </div>
                        <hr>
                        <p><strong>SUPER SET: </strong> <input type="checkbox" name="super_set" data-init-plugin="switchery" checked="checked" /></p>
                        <p><strong>SET: </strong> 
                            <select class="cs-select cs-skin-slide" name="set" data-init-plugin="cs-select">
                                @for($n=1;11>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>REPETITIONS: </strong> 
                            <select class="cs-select cs-skin-slide" name="repetitions" data-init-plugin="cs-select">
                                 @for($n=1;101>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>TEMPO: </strong><br />
                            <select class="cs-select cs-skin-slide" name="tempo_up" data-init-plugin="cs-select">
                                @for($n=1;11>$n;$n++)
                                    <option value="{{$n}}">{{$n}} up</option>
                                @endfor
                            </select> / 
                            <select class="cs-select cs-skin-slide" name="tempo_down" data-init-plugin="cs-select">
                                @for($n=1;11>$n;$n++)
                                    <option value="{{$n}}">{{$n}} down</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>SHORT OF FAILURE:</strong> 
                            <select class="cs-select cs-skin-slide" name="short_of_failure" data-init-plugin="cs-select">
                                @for($n=1;11>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <p><strong>REST: </strong> 
                            <select class="cs-select cs-skin-slide" name="rest" data-init-plugin="cs-select">
                                @for($n=1;241>$n;$n++)
                                    <option>{{$n}}</option>
                                @endfor
                            </select>
                        </p>
                        <hr />
                        <div>
                            <input type="submit" value="Save" class="btn btn-primary pull-right"/>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade slide-down" id="alert-notification" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix"></div>
                <div class="modal-body">
                    <h6 class="text-center text-uppercase">Are you sure you want to delete?</h6>
                    <div class="form-group">
                        <a href="#"  data-dismiss="modal" class="btn btn-info">CANCEL</a>
                        <input type="submit" class="pull-right btn btn-danger" value="DELETE">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

