
@extends('layouts.members-template')
@section('css')
	
    <link href="/css/tpl/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/css/tpl/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="/css/tpl/calendar.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
	     <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	  <![endif]-->
@endsection

@section('content')
	<div id="calendar_month" class="full-height"></div>
    <div class="container-fluid container-fixed-lg footer">
        <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset"> <span class="hint-text">Copyright &copy; 2014 </span> <span class="font-montserrat">REVOX</span>. <span class="hint-text">All rights reserved. </span> <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span> </p>
            <p class="small no-margin pull-right sm-pull-reset"> <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span> </p>
            <div class="clearfix"></div>
        </div>
    </div>
@endsection

@section('js')


    <!--script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script-->

    <script src="/js/tpl/pace.min.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>

    <script src="/js/tpl/jquery.scrollbar.min.js"></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/interact.min.js" type="text/javascript"></script>
    <script src="/js/tpl/moment-with-locales.min.js"></script>
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/pages.calendar.min.js"></script>
    <script src="/js/tpl/calendar_month.js" type="text/javascript"></script>
    <script src="/js/tpl/scripts.js" type="text/javascript"></script>

@endsection