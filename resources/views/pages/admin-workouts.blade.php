
@extends('layouts.admin-template')
@section('css')
    
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/tpl/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    
    <link href="/css/tpl/summernote.css" rel="stylesheet" type="text/css" media="screen">

    <!--link href="/css/tpl/dropzone.css" rel="stylesheet" type="text/css" media="screen"-->
    <link href="/css/tpl/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/admin.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
         <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
@endsection

@section('js')
    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/modernizr.custom.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/js/tpl/bootstrap.min.js" type="text/javascript"></script>
    <!--script src="/js/tpl/dropzone.min.js"></script-->
    <script src="/js/tpl/jquery.scrollbar.min.js"></script>

    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script>

    <!--script src="/js/tpl/classie.js" type="text/javascript" ></script>   
    <script src="/js/tpl/summernote.min.js" type="text/javascript"></script-->

    <!--script src="/js/tpl/select2.min.js" type="text/javascript" ></script>
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>
    <script src="/js/tpl/switchery.min.js" type="text/javascript"></script>
    <script src="/js/tpl/d3.v3.js" type="text/javascript"></script>
    <script src="/js/tpl/nv.d3.min.js" type="text/javascript"></script>
    <script src="/js/tpl/utils.js" type="text/javascript"></script>
    <script src="/js/tpl/tooltip.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/js/tpl/skycons.js" type="text/javascript"></script>
    <script src="/js/tpl/MetroJs.min.js" type="text/javascript"></script-->
    <!--script src="/js/tpl/bootstrap-datepicker.js" type="text/javascript"></script-->
    
    <script src="/js/tpl/classie.js" type="text/javascript" ></script>  
    <script src="/js/tpl/pages.min.js"></script>
    <script src="/js/tpl/page-workouts.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#description').summernote({
                height:150,
                toolbar: [
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            ['color', ['color']]
                         ],
                placeholder: 'Enter Full Description'
            });
        });
    </script>

@endsection

@section('content')
<div class="page-content-wrapper ">
    <div class="padding-25"></div>
    <div class="content">
        <div class="container-fluid container-fixed-lg">
            <div class="row">
                

                <div class="col-md-9 main-content">


                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                        
                            <!--button class="btn btn-green btn-lg pull-right bottom-right" id="btnToggleSlideUpSize">Generate</button-->
                            <a href="#" class="btn btn-default pull-right modal-popup add-workouts" data-target="#modal-slide-up">ADD PROGRAM</a>
                            <div class="panel-title">
                                <h3>PROGRAMS</h3>
                            </div>
                        </div>
                        


            <div class="row workouts">
                @if($workouts)
                    @foreach($workouts as $workout)
                        <div class="col-md-6 workout-{{$workout->id}}">

                            <div class="panel panel-default">
                                <div class="panel-heading ">
                                    <div class="panel-title">
                                       <a href="{{ url('/admin/workouts') }}/{!! $workout->url_rewrite !!}" 
                                          style="opacity: 1;color: #3E3E3E !important;text-decoration: underline;"
                                          data-toggle="popover" data-content="<span style='text-transform: none'>You can view the number of days for this program and enable to add and edit new day.</span>"> {{ $workout->name }}  <span class="fa fa-angle-double-right"></span></a>
                                    </div>
                                    <div class="panel-controls">
                                        <ul>
                                            <li>
                                                <a class="fa fa-edit modal-popup" href="#" data-toggle="tooltip" title="Edit - {{ $workout->name }}" data-id="{!! $workout->url_rewrite !!}" data-target="#modal-slide-up"></a>
                                            </li>
                                            <li>
                                                <a class="fa fa-trash notify-delete" href="#" title="Delete - {{ $workout->name }}" 
                                                    data-toggle="tooltip" 
                                                    data-id="{!! $workout->url_rewrite !!}"
                                                    data-table="program"  
                                                    data-target="#alert-notification"
                                                    data-target-delete="workout-{{$workout->id}}"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel-body" style="display: block;">
                                    <img src="http://www.s-i.lt/wp-content/uploads/2016/05/Myprotein-maisto-papildai.jpg" class="img-responsive">
                                    <hr>
                                    <div class="description">
                                        {!! $workout->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>  
                    @endforeach
                @else
                    <h1>{ Empty } </h1>
                @endif
            </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-up" id="modal-slide-up" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
                    <h5>
                        <a class="pull-right text-danger"
                           data-toggle="popover" 
                           data-content="<p><strong>FIRST: </strong> You need to set the correct value of macronutrients to generate the correct amount of ingredients per meal. </p><p><strong>SECOND: </strong> You need to select the meal ingredients per meal. We have to note that the ingredients data is relying on the ingrediets list that can be found here (<a href='/admin/ingredients/'>INGREDIENTS</a>). The ingredients must be set to the correct categories-> Protein, Carbohydrates, Fats, Condiments, Fruit/Vege.</p>
                           <p><strong>CALCULATION UNDERSTANDING:</strong> 
                           It has assumed that there is 2000 calories needed. Example we have a fish data of 105 calories/grams): </p>
                           <p>Formula for Protein calorie in Meal 1</p>

                           <p><strong>{</strong> [2000( TDCprotein% /100)] (M1protein% /100) <strong>}</strong> /105</p>"

                           data-placement="left"
                           style="font-size:15px;margin-right:15px;">
                           HELP? 
                        </a>
                        ADD PROGRAM </h5>
                    <div class="response"></div>
                </div>
                <div class="modal-body">

                    <form class="workouts-action" role="form" method="POST" action="{{ url('/admin/workouts') }}">
                       
                        <div class="response"></div>

                        {{ csrf_field() }}

                        <div class="panel panel-transparent tabs">
                            <ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                                <li class="active">
                                    <a data-toggle="tab" href="#main-wk-description"><span>Main</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#add-meals"><span>Meals</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#add-macronutrients"><span>Macronutrients</span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#help"><span>Help</span></a>
                                </li>
                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active" id="main-wk-description">
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" placeholder="Enter workout Name:" name="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Enter Full Description:</label>
                                        <div class="summernote-wrapper">
                                            <div id="description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="add-meals">
                                  


<h4 class="text-uppercase">Meal Stage 1</h4>
<div class="panel-group"role="tablist" aria-multiselectable="true">

@for($i=1; 7>$i; $i++)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" href="#meal-{{ $i }}" aria-expanded="true" aria-controls="collapseOne">
                 {{ $i }} - Meal 
                </a>
            </h4>
        </div>
        @if($i == 1)
            <div id="meal-{{ $i }}" class="panel-collapse collapse in">
        @else
            <div id="meal-{{ $i }}" class="panel-collapse collapse">
        @endif
            <div class="panel-body">
                <table class="table table-bordered meal-plan">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Primary</th>
                            <th>Secondary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="protein-calorie">
                            <th scope="row">Protein</th>
                            <td class="prime">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['protein'] AND !empty($ingredients['protein']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['protein'] as $protein)
                                        <option value="{{ $protein->calories }}|{{ $protein->id }}|{{ $protein->unit }}">{{ $protein->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                            <td class="second">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['protein'] AND !empty($ingredients['protein']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['protein'] as $protein)
                                        <option value="{{ $protein->calories }}|{{ $protein->id }}|{{ $protein->unit }}">{{ $protein->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr class="carbohydrate-calorie">
                            <th scope="row">Carbohydrates</th>
                            <td class="prime">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['carbohydrates'] AND !empty($ingredients['carbohydrates']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['carbohydrates'] as $carbohydrates)
                                        <option value="{{ $carbohydrates->calories }}|{{ $carbohydrates->id }}|{{ $carbohydrates->unit }}">{{ $carbohydrates->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                            <td class="second">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['carbohydrates'] AND !empty($ingredients['carbohydrates']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['carbohydrates'] as $carbohydrates)
                                        <option value="{{ $carbohydrates->calories }}|{{ $carbohydrates->id }}|{{ $carbohydrates->unit }}">{{ $carbohydrates->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr class="fat-calorie">
                            <th scope="row">Fats</th>
                            <td class="prime">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['fats'] AND !empty($ingredients['fats']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['fats'] as $fats)
                                        <option value="{{ $fats->calories }}|{{ $fats->id }}|{{ $fats->unit }}">{{ $fats->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                            <td class="second">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['fats'] AND !empty($ingredients['fats']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['fats'] as $fats)
                                        <option value="{{ $fats->calories }}|{{ $fats->id }}|{{ $fats->unit }}">{{ $fats->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr class="fruit-vege">
                            <th scope="row">Fruit/Vege</th>
                            <td class="prime">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['fruit_vege'] AND !empty($ingredients['fruit_vege']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['fruit_vege'] as $fruit_vege)
                                        <option value="{{ $fruit_vege->calories }}|{{ $fruit_vege->id }}|{{ $fruit_vege->unit }}">{{ $fruit_vege->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="condiment">
                            <th scope="row">Condiment</th>
                            <td class="prime">
                                <div class="form-group">
                                    <span class="help pull-right"></span><br />
                                    @if($ingredients['condiments'] AND !empty($ingredients['condiments']))
                                    <select class="cs-select cs-skin-slide data" data-init-plugin="cs-select">
                                        <option value="0|0|0">Please Select</option>
                                        @foreach($ingredients['condiments'] as $condiments)
                                        <option value="{{ $condiments->calories }}|{{ $condiments->id }}|{{ $condiments->unit }}">{{ $condiments->name }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="video-id">
                            <th scope="row">Video ID</th>
                            <td>
                                <input type="text" placeholder="YOUTUBE ID:" name="youtube_id" class="youtube-id form-control">
                            </td>
                            <td>
                                <input type="text" placeholder="VIMEO ID:"  name="vimeo_id" class="vimeo-id form-control">
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@endfor
</div>




                                </div>
                                <div class="tab-pane text-center" id="add-macronutrients">
                                    


<table class="table table-bordered macro-nutrients">
    <thead>
        <tr>
            <th></th>
            <th>Proteins %</th>
            <th>Carbohydrates%</th>
            <th>Fats %</th>
        </tr>
    </thead>
    <tbody>
        <tr class="active tdc">
            <th scope="row">TDC</th>
            <td><input type="number" placeholder="%" value="41" class="p form-control"></td>
            <td><input type="number" placeholder="%" value="30" class="c form-control"></td>
            <td><input type="number" placeholder="%" value="30" class="f form-control"></td>
        </tr>
    <?php $default_nutri = [
        1 => [ 16, 35, 20 ], 2 => [ 16, 20, 20 ], 3 => [ 16, 30, 15 ],
        4 => [ 16, 15, 15 ], 5 => [ 16, 0, 30 ], 6 => [ 16, 0, 0 ],
    ]; ?>

    @foreach($default_nutri as $dn => $dnv)
        <tr class="m{{ $dn }}">
            <th scope="row">M{{$dn }}</th>
            <td><input type="number" placeholder="%" value="{{ $dnv[0] }}" class="p form-control"></td>
            <td><input type="number" placeholder="%" value="{{ $dnv[1] }}" class="c form-control"></td>
            <td><input type="number" placeholder="%" value="{{ $dnv[2] }}" class="f form-control"></td>
        </tr>
    @endforeach
        
    </tbody>
</table>

                                </div>
                                <div class="tab-pane" id="help">

                                    <p><strong>FIRST: </strong> You need to set the correct value of macronutrients to generate the correct amount of ingredients per meal. </p>
                                    <p><strong>SECOND: </strong> You need to select the meal ingredients per meal. We have to note that the ingredients data is relying on the ingrediets list that can be found here (<a href='/admin/ingredients/'>INGREDIENTS</a>). The ingredients must be set to the correct categories-> Protein, Carbohydrates, Fats, Condiments, Fruit/Vege.</p>
                                    <p><strong>CALCULATION UNDERSTANDING:</strong> It has assumed that there is 2000 calories needed. Example we have a fish data of 105 calories/grams): </p>
                                    <p>Formula for Protein calorie in Meal 1</p>

                                    <hr />

                                    <p> TDCprotein = 2000(TDCprotein% /100) </p> 

                                    <p> Meal1protein = TDCprotein(Meal1protein% /100) </p> 
                                    
                                    <p> GramsOfFishNeeded = Meal1protein /105 </p> 
                                </div>
                            </div>
                        </div>


<div class="col-md-12 row">
    <div class="col-sm-8">

    </div>
    <div class="col-md-4 m-t-10 sm-m-t-10">
        <button type="submit" class="btn btn-primary btn-block m-t-5">SAVE</button>
    </div>
</div><div class="clearfix"></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-down" id="alert-notification" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix"></div>
                <div class="modal-body">
                    <h6 class="text-center text-uppercase">Are you sure you want to delete?</h6>
                    <div class="form-group">
                        <a href="#"  data-dismiss="modal" class="btn btn-info">CANCEL</a>
                        <input type="submit" data-dismiss="modal" class="pull-right btn btn-danger" value="DELETE">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

