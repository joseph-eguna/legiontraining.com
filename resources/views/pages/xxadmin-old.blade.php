
@extends('layouts.admin')


@section('left-sidebar')
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                
                <li>
                    <a href="#">
                        <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" class="img-responsive" />
                    </a>
                </li>
                <li>
                    <a href="#"><i class="icon-categories"></i> Overview</a>
                </li>
                <li style="position:static;">
                    <a href="#"><i class="icon-barbell"></i> Legion Workout <span class="fa arrow"></span></a>
                   <ul class="nav nav-second-level">
                        <li>
                            <a href="#panels-wells.html">Exercise</a>
                        </li>
                        <li>
                            <a href="#buttons.html">Boulders for Shoulders Workout</a>
                        </li>
                        <li>
                            <a href="#notifications.html">Ultimate Shred Workouts</a>
                        </li>
                        <li>
                            <a href="#typography.html">Pre-Conditioning Workouts</a>
                        </li>
                        <li>
                            <a href="#icons.html"> Male Workout</a>
                        </li>
                        <li>
                            <a href="#grid.html">Female Workout</a>
                        </li>
                        <li>
                            <a href="#grid.html">Hell Legs Workout</a>
                        </li>
                        <li>
                            <a href="#grid.html">Pure Bulk Workouts</a>
                        </li>
                        <li>
                            <a href="#grid.html">Back Attack</a>
                        </li>
                        <li>
                            <a href="#grid.html">Legion on The Hustle</a>
                        </li>
                        <li>
                            <a href="#grid.html">Legion Ignite</a>
                        </li>
                        <li>
                            <a href="#grid.html">Rack Attack</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-apple"></i> Legion Meals <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="#panels-wells.html">Meal Plan</a>
                        </li>
                        <li>
                            <a href="#buttons.html">Ingredients</a>
                        </li>
                    </ul>
                </li>
                

               
                <li>
                    <a href="#"><i class="icon-wallet"></i> Resources<span class="fa arrow"></span></a>
                    
                </li>
                <li>
                    <a href="#"><i class="fa fa-files-o fa-fw"></i> Legion Programs<span class="fa arrow"></span></a>
                </li>
                <li>
                    <a href="#"><i class="icon-comment-1"></i> Support<span class="fa arrow"></span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-facebook-square"></i> Social<span class="fa arrow"></span></a>
                </li>
            </ul>
        </div>
    </div>    
@endsection
