@extends('layouts.public-template')

@section('head')
    
   
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/font-awesome.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/css/tpl/registration.css" />
    

@endsection

@section('js')
    
    <script src="/js/tpl/modernizr.custom.js"></script>
    <script src="/js/tpl/classie.js"></script>
    <script src="/js/tpl/selectFx.js"></script>
    <script src="/js/tpl/fullscreenForm.js"></script>
    <script>
        (function() {
            var formWrap = document.getElementById( 'fs-form-wrap' );

            [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {    
                new SelectFx( el, {
                    stickyPlaceholder: false,
                    onChange: function(val){
                        document.querySelector('span.cs-placeholder').style.backgroundColor = val;
                    }
                });
            } );

            new FForm( formWrap, {
                onReview : function() {
                    classie.add( document.body, 'overview' ); // for demo purposes only
                }
            } );
        })();
    </script>

    <!-- END VENDOR JS -->
    <!--script src="/js/tpl/pages.min.js"></script-->
    
@endsection




@section('content')
        
<div class="container">
    <div class="fs-form-wrap" id="fs-form-wrap">

        <form id="myform" class="fs-form fs-form-full" method="POST" autocomplete="off" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="text-center">
                <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" class="img-reponsive">
            </div>

            <span class="text-danger">
                @if ($errors)
                    @if ($errors->has('name'))
                        <strong>{{ $errors->first('name') }}</strong> <br />
                    @endif
                    @if ($errors->has('email'))
                        <strong>{{ $errors->first('email') }}</strong> <br />
                    @endif
                    @if ($errors->has('password'))
                        <strong>{{ $errors->first('password') }}</strong> <br />
                    @endif
                    @if ($errors->has('password_confirmation'))
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    @endif
                @endif
            </span>

            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1">What's your name?</label>
                    <input class="fs-anim-lower" id="q1" name="name" type="text" placeholder="Joseph Eguna" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q2">What's your Email Address?</label>
                    <input class="fs-anim-lower" id="q2" name="email" type="email" placeholder="joseph.eguna@gmail.com" required/>

                     
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q3" >Create Password</label>
                    <input class="fs-anim-lower" id="q3" name="password" type="password" required/>

                    
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q4">Confirm Password</label>
                    <input class="fs-anim-lower" id="q4" name="password_confirmation" type="password" required/>

                </li>

                
                <!--li data-input-trigger>
                    <label class="fs-field-label fs-anim-upper" for="q3" data-info="This will help us know what kind of service you need">What's your priority for your new website?</label>
                    <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                        <span><input id="q3b" name="q3" type="radio" value="conversion"/><label for="q3b" class="radio-conversion">Sell things</label></span>
                        <span><input id="q3c" name="q3" type="radio" value="social"/><label for="q3c" class="radio-social">Become famous</label></span>
                        <span><input id="q3a" name="q3" type="radio" value="mobile"/><label for="q3a" class="radio-mobile">Mobile market</label></span>
                    </div>
                </li>
                <!--li data-input-trigger>
                    <label class="fs-field-label fs-anim-upper" data-info="We'll make sure to use it all over">Choose a color for your website.</label>
                    <select class="cs-select cs-skin-boxes fs-anim-lower">
                        <option value="" disabled selected>Pick a color</option>
                        <option value="#588c75" data-class="color-588c75">#588c75</option>
                        <option value="#b0c47f" data-class="color-b0c47f">#b0c47f</option>
                        <option value="#f3e395" data-class="color-f3e395">#f3e395</option>
                        <option value="#f3ae73" data-class="color-f3ae73">#f3ae73</option>
                        <option value="#da645a" data-class="color-da645a">#da645a</option>
                        <option value="#79a38f" data-class="color-79a38f">#79a38f</option>
                        <option value="#c1d099" data-class="color-c1d099">#c1d099</option>
                        <option value="#f5eaaa" data-class="color-f5eaaa">#f5eaaa</option>
                        <option value="#f5be8f" data-class="color-f5be8f">#f5be8f</option>
                        <option value="#e1837b" data-class="color-e1837b">#e1837b</option>
                        <option value="#9bbaab" data-class="color-9bbaab">#9bbaab</option>
                        <option value="#d1dcb2" data-class="color-d1dcb2">#d1dcb2</option>
                        <option value="#f9eec0" data-class="color-f9eec0">#f9eec0</option>
                        <option value="#f7cda9" data-class="color-f7cda9">#f7cda9</option>
                        <option value="#e8a19b" data-class="color-e8a19b">#e8a19b</option>
                        <option value="#bdd1c8" data-class="color-bdd1c8">#bdd1c8</option>
                        <option value="#e1e7cd" data-class="color-e1e7cd">#e1e7cd</option>
                        <option value="#faf4d4" data-class="color-faf4d4">#faf4d4</option>
                        <option value="#fbdfc9" data-class="color-fbdfc9">#fbdfc9</option>
                        <option value="#f1c1bd" data-class="color-f1c1bd">#f1c1bd</option>
                    </select>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q4">Describe how you imagine your new website</label>
                    <textarea class="fs-anim-lower" id="q4" name="q4" placeholder="Describe here"></textarea>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q5">What's your budget?</label>
                    <input class="fs-mark fs-anim-lower" id="q5" name="q5" type="number" placeholder="1000" step="100" min="100"/>
                </li-->
            </ol><!-- /fs-fields -->
            <button class="fs-submit" type="submit">Start Free Trial</button>
            <div class="clearfix"></div>
        </form><!-- /fs-form -->
    </div><!-- /fs-form-wrap -->
</div><!-- /container -->




@endsection



@section('contentx')
<div class="container r_form">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/plugins/my-login-box/images/logo.png" class="img-responsive" style="margin:auto;width: 280px;max-width: 100%;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <label for="name" class="control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <label for="password" class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
