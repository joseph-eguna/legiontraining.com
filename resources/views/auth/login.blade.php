@extends('layouts.public-template')

@section('head')
    <link href="/css/tpl/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/tpl/font-awesome.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="/css/tpl/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
        <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
@endsection

@section('js')
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>

    <script src="/js/tpl/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/tpl/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <script src="/js/tpl/pages.min.js"></script>
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>
@endsection

@section('content')


<div class="login-wrapper" style="background: linear-gradient( rgba(0, 0, 0, 0.77), rgba(0, 0, 0, 0) ), url('http://original.antiwar.com/wp-content/uploads/2015/05/spartans-what-is-your-profession.jpg');">
    <!--div class="bg-pic"> <img src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy"> </div-->
    <div class="login-container bg-transparent">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40"> 

        <!--img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/plugins/my-login-box/images/logo.png" alt="logo" data-src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/plugins/my-login-box/images/logo.png" data-src-retina="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/plugins/my-login-box/images/logo.png" width="78" height="22" style="width:200px;max-width:100%;height:auto;"-->
            
            <form id="form-login" class="p-t-15" role="form" method="POST" action="{{ url('/login') }}" >
                {{ csrf_field() }}
                <div class="text-center">
                    <img src="http://3fyfjk3b5hq21oxicu3g0kd11b8f.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/legion-logo1.png" class="img-reponsive"/>
                </div>

                
                <div class="form-group form-group-default">
                    <label>Login</label>
                    <div class="controls">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> 
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group form-group-default">
                    <label>Password</label>
                    <div class="controls">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Credentials" required>

                        @if ($errors->has('password'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 no-padding">
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="checkbox1">
                            <label for="checkbox1" style="color:#fff">Keep Me Signed in</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                         <!--a href="#{{ url('/password/reset') }}" class="text-white small">Forgot your password?</a-->
                         <a href="/register" class="text-white small">No Account Yet? Register Here</a>
                    </div>
                </div>
                <div class="row">
                    <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
                </div>
                
            </form>
        </div>
    </div>
</div>

@endsection
