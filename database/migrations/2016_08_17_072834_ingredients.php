<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ingredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_url');
            $table->string('name');
            $table->longText('description');
            $table->string('unit');
            $table->string('calories');
            $table->longText('categories');
            $table->string('max_quantity');
            $table->timestamps();
        });
    }

    /** image_url 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients');
    }
}
